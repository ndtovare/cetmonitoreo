<?php
class ControllerDriver {
    static public function ctrlShowDrivers($item, $value) {
        $respuesta = ModelDriver::mdlShowDrivers($item, $value);
        return $respuesta;
    }

    static public function ctrlEditDriver() {
        if(isset($_POST["editarNombreChofer"])) {
            $datos = array(
                "name_owner_driver_client" => $_POST["editarNombreChofer"],
                "last_name_owner_driver_client" => $_POST["editarApellidoChofer"],
                "phone_owner_driver_client" => $_POST["editarTelefonoChofer"],
                "identification_owner_driver_client" => $_POST["editarIdentificacionChofer"],
                "identification_number_owner_driver_client" => $_POST["editarNumeroIdentificacionChofer"],
                "address_owner_driver_client" => $_POST["editarDireccionChofer"],
                "reference_name_owner_driver_client" => $_POST["editarNombreReferenciaChofer"],
                "reference_last_name_owner_driver_client" => $_POST["editarApellidoReferenciaChofer"],
                "reference_phone_owner_driver_client" => $_POST["editarTelefonoReferenciaChofer"],
                "id_user" => $_SESSION["id"]
            );

            $idChofer = $_POST["idChofer"];
            $respuesta = ModelDriver::mdlEditDriver($idChofer, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El chofer ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "choferes";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteDriver() {
        if(isset($_GET["idChofer"])) {
            $datos = $_GET["idChofer"];
            $respuesta = ModelDriver::mdlDeleteDriver($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El chofer ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "choferes";
                        }
                    });
                </script>';
            }
        }
    }
}