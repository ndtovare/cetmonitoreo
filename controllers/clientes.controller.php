<?php
class ControllerClient {
    static public function ctrlInsertClient() {
        if(isset($_POST["nuevoTipoCliente"])) {
            $datos = array(
                "name_owner_driver_client" => $_POST["nuevoNombreCliente"],
                "last_name_owner_driver_client" => $_POST["nuevoApellidoCliente"],
                "phone_owner_driver_client" => $_POST["nuevoTelefonoCliente"],
                "landline_owner_client" => $_POST["nuevoTelefonoFijoCliente"],
                "identification_owner_driver_client" => $_POST["nuevaIdentificacionCliente"],
                "identification_number_owner_driver_client" => $_POST["nuevoNumeroIdentificacionCliente"],
                "address_owner_driver_client" => $_POST["nuevaDireccionCliente"],
                "email_owner_client" => $_POST["nuevoCorreoCliente"],
                "reference_name_owner_driver_client" => $_POST["nuevoNombreReferenciaCliente"],
                "reference_last_name_owner_driver_client" => $_POST["nuevoApellidoReferenciaCliente"],
                "reference_phone_owner_driver_client" => $_POST["nuevoTelefonoReferenciaCliente"],
                "type_client" => $_POST["nuevoTipoCliente"],
                "id_user" => $_SESSION["id"]
            );

            echo '<script>console.log('.json_encode($datos).')</script>';

            $respuesta = ModelClient::mdlInsertClient($datos);

            echo '<script>console.log('.json_encode($respuesta).')</script>';
                
            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El cliente ha sido registrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "clientes";
                        }
                    });
                </script>';
            }
        }
    }
}