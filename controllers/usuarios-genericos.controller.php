<?php
class ControllerGenericUser {
    static public function ctrlShowGenericUsers($item, $value) {
        $respuesta = ModelGenericUser::mdlShowGenericUsers($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertGenericUser() {
        if(isset($_POST["nuevaContrasenia"])) {

            if($_POST["seleccionaVehiculoGenerico"] == "TP") {
                $vehiculo = $_POST["nuevoTransportePublico"];
            } else if($_POST["seleccionaVehiculoGenerico"] == "VP") {
                $vehiculo = $_POST["nuevoVehiculoPrivado"];
            }
            $datos = array(
                "password_generic_user" => $_POST["nuevaContrasenia"],
                "id_vehicle" => $vehiculo
            );

            $respuesta = ModelGenericUser::mdlInsertGenericUser($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El usuario genérico ha sido registrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "usuarios-genericos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditGenericUser() {
        if(isset($_POST["editarContrasenia"])) {
            if($_POST["editarSeleccionaVehiculoG"] == "TP") {
                $vehiculo = $_POST["editarTransportePublico"];
            } else if($_POST["editarSeleccionaVehiculoG"] == "VP") {
                $vehiculo = $_POST["editarVehiculoPrivado"];
            }
            $datos = array(
                "password_generic_user" => $_POST["editarContrasenia"],
                "id_vehicle" => $vehiculo
            );

            $idUsuarioGenerico = $_POST["idUsuarioGenerico"];

            $respuesta = ModelGenericUser::mdlEditGenericUser($idUsuarioGenerico, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El usuario genérico ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "usuarios-genericos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteGenericUser() {
        if(isset($_GET["idUsuarioGenerico"])) {
            $datos = $_GET["idUsuarioGenerico"];
            $respuesta = ModelGenericUser::mdlDeleteGenericUser($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El usuario genérico ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "usuarios-genericos";
                        }
                    });
                </script>';
            }
        }
    }
}