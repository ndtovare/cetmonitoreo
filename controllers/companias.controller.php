<?php
class ControllerCompany {
    static public function ctrlShowCompanies($item, $value) {
        $respuesta = ModelCompany::mdlShowCompanies($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertCompany() {
        if(isset($_POST["nuevoNombreCompania"])) {
            $validaAcronimo = ModelCompany::mdlValidateCompany($_POST["nuevoAcronimoCompania"]);

            if($validaAcronimo == 0) {
                $datos = array(
                    "name_business" => $_POST["nuevoNombreCompania"],
                    "acronym_business" => $_POST["nuevoAcronimoCompania"],
                    "phone_business" => $_POST["nuevoTelefonoCompania"],
                    "representative_email_business" => $_POST["nuevoCorreoRepresentanteCompania"],
                    "address_business" => $_POST["nuevaDireccionCompania"],
                    "email_business" => $_POST["nuevoCorreoCompania"],
                    "representative_name_business" => $_POST["nuevoNombreRepresentanteCompania"],
                    "representative_last_name_business" => $_POST["nuevoApellidoRepresentanteCompania"],
                    "representative_phone_business" => $_POST["nuevoTelefonoRepresentanteCompania"],
                    "reference_name_business" => $_POST["nuevoNombreReferenciaCompania"],
                    "reference_last_name_business" => $_POST["nuevoApellidoReferenciaCompania"],
                    "reference_phone_business" => $_POST["nuevoTelefonoReferenciaCompania"],
                    "id_user" => $_SESSION["id"]
                );
                
                $respuesta = ModelCompany::mdlInsertCompany($datos);
    
                if($respuesta == "ok") {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "success",
                            title: "¡La compañía ha sido registrada correctamente!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        }).then((result) => {
                            if(result.value) {
                                window.location = "companias";
                            }
                        });
                    </script>';
                }
            } else {
                echo '
                <script>
                    Swal.fire({
                        icon: "warning",
                        title: "¡El acrónimo ya está registrado",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "companias";
                        }
                    });
                </script>';
            }

            
        }
    }

    static public function ctrlEditCompany() {
        if(isset($_POST["editarNombreCompania"])) {
            $datos = array(
                "name_business" => $_POST["editarNombreCompania"],
                "acronym_business" => $_POST["editarAcronimoCompania"],
                "phone_business" => $_POST["editarTelefonoCompania"],
                "representative_email_business" => $_POST["editarCorreoRepresentanteCompania"],
                "address_business" => $_POST["editarDireccionCompania"],
                "email_business" => $_POST["editarCorreoCompania"],
                "representative_name_business" => $_POST["editarNombreRepresentanteCompania"],
                "representative_last_name_business" => $_POST["editarApellidoRepresentanteCompania"],
                "representative_phone_business" => $_POST["editarTelefonoRepresentanteCompania"],
                "reference_name_business" => $_POST["editarNombreReferenciaCompania"],
                "reference_last_name_business" => $_POST["editarApellidoReferenciaCompania"],
                "reference_phone_business" => $_POST["editarTelefonoReferenciaCompania"],
                "id_user" => $_SESSION["id"]
            );

            $idCompania = $_POST["idCompania"];
            $respuesta = ModelCompany::mdlEditCompany($idCompania, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La compañía ha sido editada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "companias";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteCompany() {
        if(isset($_GET["idCompania"])) {
            $datos = $_GET["idCompania"];
            $respuesta = ModelCompany::mdlDeleteCompany($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La compañía ha sido borrada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "companias";
                        }
                    });
                </script>';
            }
        }
    }
}