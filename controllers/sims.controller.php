<?php
class ControllerSim {
    static public function ctrlShowSims($item, $value) {
        $respuesta = ModelSim::mdlShowSims($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertSim() {
        if(isset($_POST["nuevoNumeroSerie"])) {
            $telefonoValidado = ModelSim::mdlValidateSim($_POST["nuevoNumeroSerie"]);

            if($telefonoValidado == 0) {
                $fechaSimOriginal = $_POST["nuevaFecha"];
                $fechaCorteSimOriginal = $_POST["nuevaFechaCorte"];
                $timeStampFecha = strtotime($fechaSimOriginal);
                $timeStampCorte = strtotime($fechaCorteSimOriginal);
                $fechaSimNueva = date("Y-m-d", $timeStampFecha);
                $fechaCorteSimNueva = date("Y-m-d", $timeStampCorte);
                
                $datos = array(
                    "status_sim" => $_POST["nuevoEstatus"],
                    "serial_number_sim" => $_POST["nuevoNumeroSerie"],
                    "number_sim" => $_POST["nuevoNumeroSim"],
                    "scheme_sim" => $_POST["nuevoEsquema"],
                    "balance_sim" => $_POST["nuevoSaldo"],
                    "date_sim" => $fechaSimNueva,
                    "cutting_day_sim" => $fechaCorteSimNueva,
                    "id_code" => $_POST["nuevoCodigo"],
                    "id_lot" => $_POST["nuevoLote"],
                    "id_user" => $_SESSION["id"]
                );
    
                $respuesta = ModelSim::mdlInsertSim($datos);
    
                if($respuesta == "ok") {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "success",
                            title: "¡El sim ha sido registrado correctamente!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        }).then((result) => {
                            if(result.value) {
                                window.location = "sims";
                            }
                        });
                    </script>';
                }
            } else {
                echo '
                <script>
                    Swal.fire({
                        icon: "warning",
                        title: "¡El número de serie ya está registrado!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditSim() {
        if(isset($_POST["editarNumeroSerie"])) {
            $idSim = $_POST["idSim"];
            $fechaSimOriginal = $_POST["editarFecha"];
            $fechaCorteSimOriginal = $_POST["editarFechaCorte"];
            $timeStampFecha = strtotime($fechaSimOriginal);
            $timeStampCorte = strtotime($fechaCorteSimOriginal);
            $fechaSimNueva = date("Y-m-d", $timeStampFecha);
            $fechaCorteSimNueva = date("Y-m-d", $timeStampCorte);
            
            $datos = array(
                "status_sim" => $_POST["editarEstatus"],
                "serial_number_sim" => $_POST["editarNumeroSerie"],
                "number_sim" => $_POST["editarNumeroSim"],
                "scheme_sim" => $_POST["editarEsquema"],
                "balance_sim" => $_POST["editarSaldo"],
                "date_sim" => $fechaSimNueva,
                "cutting_day_sim" => $fechaCorteSimNueva,
                "id_code" => $_POST["editarCodigo"],
                "id_lot" => $_POST["editarLote"],
                "id_user" => $_SESSION["id"]
            );

            echo '<script>console.log('.json_encode($datos).')</script>';
            echo '<script>console.log('.json_encode($idSim).')</script>';
            
            $respuesta = ModelSim::mdlEditSim($idSim, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El sim ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "sims";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteSim() {
        if(isset($_GET["idSim"])) {
            $datos = $_GET["idSim"];
            $respuesta = ModelSim::mdlDeleteSim($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El sim ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "sims";
                        }
                    });
                </script>';
            }
        }
    }
}