<?php
class ControllerOwner {
    static public function ctrlShowOwners($item, $value) {
        $respuesta = ModelOwner::mdlShowOwners($item, $value);
        return $respuesta;
    }

    static public function ctrlEditOwner() {
        if(isset($_POST["editarNombrePropietario"])) {
            $datos = array(
                "name_owner_driver_client" => $_POST["editarNombrePropietario"],
                "last_name_owner_driver_client" => $_POST["editarApellidoPropietario"],
                "phone_owner_driver_client" => $_POST["editarTelefonoPropietario"],
                "landline_owner_client" => $_POST["editarTelefonoFijoPropietario"],
                "identification_owner_driver_client" => $_POST["editarIdentificacionPropietario"],
                "identification_number_owner_driver_client" => $_POST["editarNumeroIdentificacionPropietario"],
                "address_owner_driver_client" => $_POST["editarDireccionPropietario"],
                "email_owner_client" => $_POST["editarCorreoPropietario"],
                "reference_name_owner_driver_client" => $_POST["editarNombreReferenciaPropietario"],
                "reference_last_name_owner_driver_client" => $_POST["editarApellidoReferenciaPropietario"],
                "reference_phone_owner_driver_client" => $_POST["editarTelefonoReferenciaPropietario"],
                "id_user" => $_SESSION["id"]
            );

            $idPropietario = $_POST["idPropietario"];
            $respuesta = ModelOwner::mdlEditOwner($idPropietario, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El propietario ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "propietarios";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteOwner() {
        if(isset($_GET["idPropietario"])) {
            $datos = $_GET["idPropietario"];
            $respuesta = ModelOwner::mdlDeleteOwner($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El propietario ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "propietarios";
                        }
                    });
                </script>';
            }
        }
    }
}