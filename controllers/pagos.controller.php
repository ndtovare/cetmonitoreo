<?php
class ControllerPayment {
    static public function ctrlShowPayments($item, $value) {
        $respuesta = ModelPayment::mdlShowPayments($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertPayment() {
        if(isset($_POST["nuevoIdentificadorPago"])) {
            $esValido = ModelPayment::mdlValidatePayment($_POST["nuevoIdentificadorPago"]);

            if($esValido == 0) {
                
                $fechaCompraOriginal = $_POST["nuevaFechaPago"];
                $timeStampCompra = strtotime($fechaCompraOriginal);
                $fechaCompraNueva = date("Y-m-d", $timeStampCompra);
                
                $datos = array(
                    "identifier_payment" => $_POST["nuevoIdentificadorPago"],
                    "month_payment" => $_POST["nuevoMes"],
                    "method_payment" => $_POST["nuevoMetodoPago"],
                    "number_payment" => $_POST["nuevoNumeroPago"],
                    "date_payment" => $fechaCompraNueva,
                    "amount_payment" => $_POST["nuevoMontoPago"]
                );
    
                echo '<script>console.log('.json_encode($datos).')</script>';
    
                $respuesta = ModelPayment::mdlInsertPayment($datos);
    
                if($respuesta == "ok") {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "success",
                            title: "¡El pago ha sido registrado correctamente!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        }).then((result) => {
                            if(result.value) {
                                window.location = "pagos";
                            }
                        });
                    </script>';
                }
            }
            
        }
    }

    static public function ctrlEditPayment() {
        if(isset($_POST["editarIdentificadorPago"])) {

            $idPago = $_POST["idPago"];
            $fechaCompraOriginal = $_POST["editarFechaPago"];
            $timeStampCompra = strtotime($fechaCompraOriginal);
            $fechaCompraNueva = date("Y-m-d", $timeStampCompra);

            $datos = array(
                "identifier_payment" => $_POST["editarIdentificadorPago"],
                "month_payment" => $_POST["editarMes"],
                "method_payment" => $_POST["editarMetodoPago"],
                "number_payment" => $_POST["editarNumeroPago"],
                "date_payment" => $fechaCompraNueva,
                "amount_payment" => $_POST["editarMontoPago"]
            );
            
            $respuesta = ModelPayment::mdlEditPayment($idPago, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El pago ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "pagos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeletePayment() {
        if(isset($_GET["idPago"])) {
            $datos = $_GET["idPago"];
            $respuesta = ModelPayment::mdlDeletePayment($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El pago ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "pagos";
                        }
                    });
                </script>';
            }
        }
    }
}