<?php
class ControllerPrivateVehicle {
    static public function ctrlShowPrivateVehicles($item, $value) {
        $respuesta = ModelPrivateVehicle::mdlShowPrivateVehicles($item, $value);
        return $respuesta;
    }

    static public function ctrlEditPrivateVehicle() {
        if(isset($_POST["editarImeiTP"])) {
            $datos = array(
                "imei_private_public_vehicle" => $_POST["editarImeiTP"],
                "identifier_private_public_vehicle" => $_POST["editarIdentificadorTP"],
                "brand_private_public_vehicle" => $_POST["editarMarcaTP"],
                "model_private_public_vehicle" => $_POST["editarModeloTP"],
                "year_private_public_vehicle" => $_POST["editarAnioTP"],
                "vehicular_series_private_public_vehicle" => $_POST["editarSerieTP"],
                "plate_number_private_public_vehicle" => $_POST["editarPlacaTP"],
                "class_private_public_vehicle" => $_POST["editarTipoTP"],
                "id_user" => $_SESSION["id"],
                "id_owner" => $_POST["editarPropietario"],
                "id_driver" => $_POST["editarChofer"]
            );

            $idVehiculoPrivado = $_POST["idVehiculoPrivado"];
            $respuesta = ModelPrivateVehicle::mdlEditPrivateVehicle($idVehiculoPrivado, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El vehículo privado ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "vehiculos-privados";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeletePrivateVehicle() {
        if(isset($_GET["idVehiculoPrivado"])) {
            $datos = $_GET["idVehiculoPrivado"];
            $respuesta = ModelPrivateVehicle::mdlDeletePrivateVehicle($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El vehículo privado ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "vehiculos-privados";
                        }
                    });
                </script>';
            }
        }
    }
}