<?php
class ControllerFleet {
    static public function ctrlShowFleets($item, $value) {
        $respuesta = ModelFleet::mdlShowFleets($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertFleet() {
        if(isset($_POST["nuevoNombreFlotilla"])) {

            if($_POST["seleccionaVehiculoFlotilla"] == "TP") {
                $vehiculo = $_POST["nuevoTransportePublico"];
            } else if($_POST["seleccionaVehiculoFlotilla"] == "VP") {
                $vehiculo = $_POST["nuevoVehiculoPrivado"];
            }

            $datos = array(
                "name_fleet" => $_POST["nuevoNombreFlotilla"],
                "address_fleet" => $_POST["nuevaDireccionFlotilla"],
                "phone_contact_fleet" => $_POST["nuevoTelefonoFlotilla"],
                "name_contact_fleet" => $_POST["nuevoNombreContactoFlotilla"],
                "last_name_contact_fleet" => $_POST["nuevoApellidoContactoFlotilla"],
                "id_business" => $_POST["nuevaCompania"],
                "id_vehicle" => $vehiculo
            );

            echo '<script>console.log('.json_encode($datos).')</script>';
            
            $respuesta = ModelFleet::mdlInsertFleet($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La flotilla ha sido registrada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "flotillas";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditFleet() {
        if(isset($_POST["editarNombreFlotilla"])) {
            if($_POST["editarSeleccionaVehiculoF"] == "TP") {
                $vehiculo = $_POST["editarTransportePublico"];
            } else if($_POST["editarSeleccionaVehiculoF"] == "VP") {
                $vehiculo = $_POST["editarVehiculoPrivado"];
            }

            $datos = array(
                "name_fleet" => $_POST["editarNombreFlotilla"],
                "address_fleet" => $_POST["editarDireccionFlotilla"],
                "phone_contact_fleet" => $_POST["editarTelefonoFlotilla"],
                "name_contact_fleet" => $_POST["editarNombreContactoFlotilla"],
                "last_name_contact_fleet" => $_POST["editarApellidoContactoFlotilla"],
                "id_business" => $_POST["editarCompania"],
                "id_vehicle" => $vehiculo
            );

            echo '<script>console.log('.json_encode($datos).')</script>';

            $respuesta = ModelFleet::mdlEditFleet($_POST["idFlotilla"], $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La flotilla ha sido editada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "flotillas";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteFleet() {
        if(isset($_GET["idFlotilla"])) {
            $datos = $_GET["idFlotilla"];
            $respuesta = ModelFleet::mdlDeleteFleet($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La flotilla ha sido borrada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "flotillas";
                        }
                    });
                </script>';
            }
        }
    }
}