<?php
class ControllerInstalledEquipment {
    static public function ctrlShowInstalledEquipment($item, $value) {
        $respuesta = ModelInstalledEquipment::mdlShowInstalledEquipment($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertInstalledEquipment() {
        if(isset($_POST["nuevaDescripción"])) {
            if($_POST["seleccionaVehiculoEquipoInstalado"] == "TP") {
                $vehiculo = $_POST["nuevoTransportePublico"];
            } else if($_POST["seleccionaVehiculoEquipoInstalado"] == "VP") {
                $vehiculo = $_POST["nuevoVehiculoPrivado"];
            }

            $fechaInstalacionOriginal = $_POST["nuevaFechaInstalacion"];
            $timeStampInstalacion = strtotime($fechaInstalacionOriginal);
            $fechaInstalacionNueva = date("Y-m-d", $timeStampInstalacion);
            $idProducto = $_POST["idProducto"];

            $datos = array(
                "description_installed_equipment" => $_POST["nuevaDescripción"],
                "comments_installed_equipment" => $_POST["nuevoComentario"],
                "installation_date_installed_equipment" => $fechaInstalacionNueva,
                "id_lot" => $_POST["nuevoLote"],
                "id_code" => $_POST["nuevoCodigo"],
                "id_vehicle" => $vehiculo,
                "id_product" => $_POST["idProducto"],
                "serial_number_product" => $_POST["nuevoSerieProducto"],
                "id_user" => $_SESSION["id"]
            );

            echo '<script>console.log('.json_encode($datos).')</script>';
            
            $respuesta = ModelInstalledEquipment::mdlInsertInstalledEquipment($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El equipo instalado ha sido registrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "equipos-instalados";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditInstalledEquipment() {
        if(isset($_POST["editarDescripción"])) {

            if($_POST["editarSeleccionaVehiculoEI"] == "TP") {
                $vehiculo = $_POST["editarTransportePublico"];
            } else if($_POST["editarSeleccionaVehiculoEI"] == "VP") {
                $vehiculo = $_POST["editarVehiculoPrivado"];
            }

            $id = $_POST["idEquipoInstalado"];

            $fechaInstalacionOriginal = $_POST["editarFechaInstalacion"];
            $timeStampInstalacion = strtotime($fechaInstalacionOriginal);
            $fechaInstalacionNueva = date("Y-m-d", $timeStampInstalacion);
            $datos = array(
                "description_installed_equipment" => $_POST["editarDescripción"],
                "comments_installed_equipment" => $_POST["editarComentario"],
                "installation_date_installed_equipment" => $fechaInstalacionNueva,
                "id_lot" => $_POST["editarLote"],
                "id_code" => $_POST["editarCodigo"],
                "id_vehicle" => $vehiculo,
                "id_user" => $_SESSION["id"]
            );
            
            $respuesta = ModelInstalledEquipment::mdlEditInstalledEquipment($id, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El equipo instalado ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "equipos-instalados";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteInstalledEquipment() {
        if(isset($_GET["idEquipoInstalado"])) {
            $datos = $_GET["idEquipoInstalado"];
            echo '<script>console.log('.json_encode($datos).')</script>';
            $respuesta = ModelInstalledEquipment::mdlDeleteInstalledEquipment($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El equipo instalado ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "equipos-instalados";
                        }
                    });
                </script>';
            }
        }
    }
}