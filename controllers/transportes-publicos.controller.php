<?php
class ControllerPublicTransport {
    static public function ctrlShowPublicTransports($item, $value) {
        $respuesta = ModelPublicTransport::mdlShowPublicTransports($item, $value);
        return $respuesta;
    }

    static public function ctrlEditPublicTransport() {
        if(isset($_POST["editarImeiTP"])) {
            $datos = array(
                "imei_private_public_vehicle" => $_POST["editarImeiTP"],
                "identifier_private_public_vehicle" => $_POST["editarIdentificadorTP"],
                "brand_private_public_vehicle" => $_POST["editarMarcaTP"],
                "model_private_public_vehicle" => $_POST["editarModeloTP"],
                "year_private_public_vehicle" => $_POST["editarAnioTP"],
                "vehicular_series_private_public_vehicle" => $_POST["editarSerieTP"],
                "economic_number_public_vehicle" => $_POST["editarEconomicoTP"],
                "plate_number_private_public_vehicle" => $_POST["editarPlacaTP"],
                "class_private_public_vehicle" => $_POST["editarTipoTP"],
                "id_user" => $_SESSION["id"],
                "id_owner" => $_POST["editarPropietario"],
                "id_driver" => $_POST["editarChofer"],
                "id_business" => $_POST["editarCompania"]
            );

            $idTransportePublico = $_POST["idTransportePublico"];
            $respuesta = ModelPublicTransport::mdlEditPublicTransport($idTransportePublico, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El transporte público ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "transportes-publicos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeletePublicTransport() {
        if(isset($_GET["idTransportePublico"])) {
            $datos = $_GET["idTransportePublico"];
            $respuesta = ModelPublicTransport::mdlDeletePublicTransport($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El transporte público ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "transportes-publicos";
                        }
                    });
                </script>';
            }
        }
    }
}