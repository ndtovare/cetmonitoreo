<?php
class ControllerProduct {
    static public function ctrlShowProducts($item, $value, $isInstalled) {
        $respuesta = ModelProduct::mdlShowProducts($item, $value, $isInstalled);
        return $respuesta;
    }

    static public function ctrlInsertProduct() {
        if(isset($_POST["nuevoNumeroSerie"])) {
            $esValido = ModelProduct::mdlValidateProduct($_POST["nuevoNumeroSerie"], $_POST["nuevoCodigo"]);

            if($esValido == 0) {
                $datos = array(
                    "status_product" => $_POST["nuevoEstatus"],
                    "serial_number_product" => $_POST["nuevoNumeroSerie"],
                    "id_code" => $_POST["nuevoCodigo"],
                    "id_lot" => $_POST["nuevoLote"],
                    "id_user" => $_SESSION["id"]
                );
                
                $respuesta = ModelProduct::mdlInsertProduct($datos);
    
                if($respuesta == "ok") {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "success",
                            title: "¡El producto ha sido registrado correctamente!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        }).then((result) => {
                            if(result.value) {
                                window.location = "productos";
                            }
                        });
                    </script>';
                } else {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "success",
                            title: "¡No se ha seleccionado ningún lote o no hay lotes registrados!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        }).then((result) => {
                            if(result.value) {
                                window.location = "productos";
                            }
                        });
                    </script>';
                }
            } else {
                echo '
                <script>
                    Swal.fire({
                        icon: "warning",
                        title: "¡El número de serie y el código ya están registrados!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditProduct() {
        if(isset($_POST["editarNumeroSerie"])) {
            $datos = array(
                "status_product" => $_POST["editarEstatus"],
                "serial_number_product" => $_POST["editarNumeroSerie"],
                "id_code" => $_POST["editarCodigo"],
                "id_lot" => $_POST["editarLote"],
                "id_user" => $_SESSION["id"]
            );

            $idProducto = $_POST["idProducto"];
            $respuesta = ModelProduct::mdlEditProduct($idProducto, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El producto ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "productos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteProduct() {
        if(isset($_GET["idProducto"])) {
            $datos = $_GET["idProducto"];
            $respuesta = ModelProduct::mdlDeleteProduct($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El producto ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "productos";
                        }
                    });
                </script>';
            }
        }
    }
}