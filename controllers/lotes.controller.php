<?php
class ControllerLot {
    static public function ctrlValidateLots() {
        $respuesta = ModelLot::mdlValidateLots();
        return $respuesta;
    }

    static public function ctrlShowLots($item, $value) {
        $respuesta = ModelLot::mdlShowLots($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertLot() {
        if(isset($_POST["nuevaFactura"])) {
            $fechaAdmisionOriginal = $_POST["nuevaFechaAdmision"];
            $fechaGarantiaOriginal = $_POST["nuevaFechaGarantia"];
            $timeStampAdmision = strtotime($fechaAdmisionOriginal);
            $timeStampGarantia = strtotime($fechaGarantiaOriginal);
            $fechaAdmisionNueva = date("Y-m-d", $timeStampAdmision);
            $fechaGarantiaNueva = date("Y-m-d", $timeStampGarantia);
            
            $datos = array(
                "bill_lot" => $_POST["nuevaFactura"],
                "date_admission_lot" => $fechaAdmisionNueva,
                "warranty_date_lot" => $fechaGarantiaNueva,
                "id_user" => $_SESSION["id"]
            );

            $respuesta = ModelLot::mdlInsertLot($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El lote ha sido registrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "lotes";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditLot() {
        if(isset($_POST["editarFactura"])) {
            $id = $_POST["idLote"];
            $fechaAdmisionOriginal = $_POST["editarFechaAdmision"];
            $fechaGarantiaOriginal = $_POST["editarFechaGarantia"];
            $timeStampAdmision = strtotime($fechaAdmisionOriginal);
            $timeStampGarantia = strtotime($fechaGarantiaOriginal);
            $fechaAdmisionNueva = date("Y-m-d", $timeStampAdmision);
            $fechaGarantiaNueva = date("Y-m-d", $timeStampGarantia);
            $datos = array(
                "bill_lot" => $_POST["editarFactura"],
                "date_admission_lot" => $fechaAdmisionNueva,
                "warranty_date_lot" => $fechaGarantiaNueva,
                "id_user" => $_SESSION["id"]
            );
            
            $respuesta = ModelLot::mdlEditLot($id, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El lote ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "lotes";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteLot() {
        if(isset($_GET["idLote"])) {
            $datos = $_GET["idLote"];
            $respuesta = ModelLot::mdlDeleteLot($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El lote ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "lotes";
                        }
                    });
                </script>';
            }
        }
    }
}