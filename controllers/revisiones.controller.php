<?php
class ControllerRevision {
    static public function ctrlShowRevisions($item, $value) {
        $respuesta = ModelRevision::mdlShowRevisions($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertRevision() {
        if(isset($_POST["nuevoIdentificador"])) {
            $fechaRevisionOriginal = $_POST["nuevaFechaRevision"];
            $timeStampRevision = strtotime($fechaRevisionOriginal);
            $fechaRevisionNueva = date("Y-m-d", $timeStampRevision);
            
            $datos = array(
                "identifier_revision" => $_POST["nuevoIdentificador"],
                "comments_revision" => $_POST["nuevoComentario"],
                "date_revision" => $fechaRevisionNueva,
                "price_revision" => $_POST["nuevoPrecio"],
                "id_installed_equipment" => $_POST["nuevoEquipoInstalado"]
            );

            echo '<script>console.log('.json_encode($datos).')</script>';

            $respuesta = ModelRevision::mdlInsertRevision($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La revisión ha sido registrada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "revisiones";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditRevision() {
        if(isset($_POST["editarIdentificador"])) {
            $id = $_POST["idRevision"];

            $fechaRevisionOriginal = $_POST["editarFechaRevision"];
            $timeStampRevision = strtotime($fechaRevisionOriginal);
            $fechaRevisionNueva = date("Y-m-d", $timeStampRevision);

            $datos = array(
                "identifier_revision" => $_POST["editarIdentificador"],
                "comments_revision" => $_POST["editarComentario"],
                "date_revision" => $fechaRevisionNueva,
                "price_revision" => $_POST["editarPrecio"],
                "id_installed_equipment" => $_POST["editarEquipoInstalado"]
            );
            
            $respuesta = ModelRevision::mdlEditRevision($id, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La revision ha sido editada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "revisiones";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteRevision() {
        if(isset($_GET["idRevision"])) {
            $datos = $_GET["idRevision"];
            $respuesta = ModelRevision::mdlDeleteRevision($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡La revisión ha sido borrada correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "revisiones";
                        }
                    });
                </script>';
            }
        }
    }
}