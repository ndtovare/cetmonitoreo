<?php
class ControllerVehicle {
    static public function ctrlInsertVehicle() {
        if(isset($_POST["nuevoTipoVehiculo"])) {
            $validarImei = ModelVehicle::mdlValidateVehicle("imei_private_public_vehicle", $_POST["nuevoImeiVehiculo"]);

            if($validarImei == 0) {
                $validarIdentificador = ModelVehicle::mdlValidateVehicle("identifier_private_public_vehicle", $_POST["nuevoIdentificadorVehiculo"]);

                if($validarIdentificador == 0) {
                    $validarSerie = ModelVehicle::mdlValidateVehicle("vehicular_series_private_public_vehicle", $_POST["nuevaSerieVehiculo"]);
                    
                    if($validarSerie == 0) {
                        $validarPlaca = ModelVehicle::mdlValidateVehicle("plate_number_private_public_vehicle", $_POST["nuevaPlacaVehiculo"]);

                        if($validarPlaca == 0) {
                            if($_POST["nuevoEconomicoVehiculo"] == "") {
                                $_POST["nuevoEconomicoVehiculo"] = null;
                            }
                            $datos = array(
                                "imei_private_public_vehicle" => $_POST["nuevoImeiVehiculo"],
                                "identifier_private_public_vehicle" => $_POST["nuevoIdentificadorVehiculo"],
                                "brand_private_public_vehicle" => $_POST["nuevaMarcaVehiculo"],
                                "model_private_public_vehicle" => $_POST["nuevoModeloVehiculo"],
                                "year_private_public_vehicle" => $_POST["nuevoAnioVehiculo"],
                                "vehicular_series_private_public_vehicle" => $_POST["nuevaSerieVehiculo"],
                                "plate_number_private_public_vehicle" => $_POST["nuevaPlacaVehiculo"],
                                "class_private_public_vehicle" => $_POST["nuevaClaseVehiculo"],
                                "economic_number_public_vehicle" => $_POST["nuevoEconomicoVehiculo"],
                                "type_vehicle" => $_POST["nuevoTipoVehiculo"],
                                "id_user" => $_SESSION["id"],
                                "id_owner" => $_POST["nuevoPropietario"],
                                "id_driver" => $_POST["nuevoChofer"],
                                "id_business" => $_POST["nuevaCompania"]
                            );
                            
                            $respuesta = ModelVehicle::mdlInsertVehicle($datos);
                
                            if($respuesta == "ok") {
                                echo '
                                <script>
                                    Swal.fire({
                                        icon: "success",
                                        title: "¡El vehículo ha sido registrado correctamente!",
                                        showConfirmButton: true,
                                        confirmButtonText: "Cerrar",
                                        closeOnConfirm: false
                                    }).then((result) => {
                                        if(result.value) {
                                            window.location = "vehiculos";
                                        }
                                    });
                                </script>';
                            }
                        } else {
                            echo '
                            <script>
                                Swal.fire({
                                    icon: "warning",
                                    title: "¡El número de placa ya está registrado!",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                });
                            </script>';
                        }
                        
                    } else {
                        echo '
                        <script>
                            Swal.fire({
                                icon: "warning",
                                title: "¡El número de serie ya está registrado!",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                            });
                        </script>';
                    }
                    
                } else {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "warning",
                            title: "¡El identificador ya está registrado!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                    </script>';
                }
            } else {
                echo '
                <script>
                    Swal.fire({
                        icon: "warning",
                        title: "¡El IMEI ya está registrado!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                </script>';
            }

            
        }
    }
}