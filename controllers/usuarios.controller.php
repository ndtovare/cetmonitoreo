<?php
class ControllerUser {
    static public function ctrlLoginUser() {
        if(isset($_POST["ingUsuario"])) {
            $valor = $_POST["ingUsuario"];
            echo '<script>console.log('.json_encode($valor).')</script>';
            $respuesta = ModelUser::mdlLoginUser($valor);

            echo '<script>console.log('.json_encode($respuesta).')</script>';
            
            if($respuesta["nickname_user"] == $_POST["ingUsuario"] && 
                $respuesta["password_user"] == $_POST["ingPassword"]) {
                $_SESSION["iniciarSesion"] = "ok";
                $_SESSION["id"] = $respuesta["id_user"];
                $_SESSION["nombre"] = $respuesta["name_user"];
                $_SESSION["usuario"] = $respuesta["nickname_user"];
                $_SESSION["foto"] = $respuesta["image_user"];
                $_SESSION["perfil"] = $respuesta["role_user"];
                echo '<script>window.location = "inicio";</script>';
            } else {
                echo '<br><div class="alert alert-danger">Error al ingresar, vuelve a intentarlo</div>';
            }
        }
    }

    static public function ctrlInsertUser() {
        if(isset($_POST["nuevoUsuario"])) {
            $ruta = "";

            if(isset($_FILES["nuevaFoto"]["tmp_name"])) {
                list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);
                $nuevoAncho = 500;
                $nuevoAlto = 500;
                $directorio = "views/img/usuarios/".$_POST["nuevoUsuario"];
                mkdir($directorio, 0755);
                
                if($_FILES["nuevaFoto"]["type"] == "image/jpeg") {
                    $aleatorio = mt_rand(100, 999);
                    $ruta = "views/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".jpg";
                    $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagejpeg($destino, $ruta);
                }

                if($_FILES["nuevaFoto"]["type"] == "image/png") {
                    $aleatorio = mt_rand(100, 999);
                    $ruta = "views/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".png";
                    $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagepng($destino, $ruta);
                }
            }

            $datos = array(
                "nickname_user" => $_POST["nuevoUsuario"],
                "password_user" => $_POST["nuevaContrasenia"],
                "name_user" => $_POST["nuevoNombre"],
                "last_name_user" => $_POST["nuevoApellido"],
                "phone_user" => $_POST["nuevoTelefono"],
                "reference_name_user" => $_POST["nuevoNombreReferencia"],
                "reference_last_name_user" => $_POST["nuevoApellidoReferencia"],
                "reference_phone_user" => $_POST["nuevoTelefonoReferencia"],
                "address_user" => $_POST["nuevaDireccion"],
                "email_user" => $_POST["nuevoCorreo"],
                "image_user" => $ruta,
                "role_user" => $_POST["nuevoRol"]
            );

            $respuesta = ModelUser::mdlInsertUser($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El usuario ha sido registrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "usuarios";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlShowUsers($item, $value) {
        $respuesta = ModelUser::mdlShowUsers($item, $value);
        return $respuesta;
    }

    static public function ctrlEditUser() {
        if(isset($_POST["editarUsuario"])) {
            $ruta = $_POST["fotoActual"];

            if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"])) {
                list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);
                $nuevoAncho = 500;
                $nuevoAlto = 500;
                $directorio = "views/img/usuarios/".$_POST["editarUsuario"];

                if(!empty($_POST["fotoActual"])) {
                    unlink($_POST["fotoActual"]);
                } else {
                    mkdir($directorio, 0755);
                }
                
                if($_FILES["editarFoto"]["type"] == "image/jpeg") {
                    $aleatorio = mt_rand(100, 999);
                    $ruta = "views/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".jpg";
                    $origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagejpeg($destino, $ruta);
                }

                if($_FILES["editarFoto"]["type"] == "image/png") {
                    $aleatorio = mt_rand(100, 999);
                    $ruta = "views/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".png";
                    $origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagepng($destino, $ruta);
                }
            }

            $datos = array(
                "nickname_user" => $_POST["editarUsuario"],
                "password_user" => $_POST["editarContrasenia"],
                "name_user" => $_POST["editarNombre"],
                "last_name_user" => $_POST["editarApellido"],
                "phone_user" => $_POST["editarTelefono"],
                "reference_name_user" => $_POST["nombreReferenciaActual"],
                "reference_last_name_user" => $_POST["apellidoReferenciaActual"],
                "reference_phone_user" => $_POST["telefonoReferenciaActual"],
                "address_user" => $_POST["direccionActual"],
                "email_user" => $_POST["editarCorreo"],
                "image_user" => $ruta,
                "role_user" => $_POST["editarRol"]
            );

            $idUsuario = $_POST["idUsuario"];
            $respuesta = ModelUser::mdlEditUser($idUsuario, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El usuario ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "usuarios";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteUser() {
        if(isset($_GET["idUsuario"])) {
            $datos = $_GET["idUsuario"];

            if($_GET["fotoUsuario"] != "") {
                unlink($_GET["fotoUsuario"]);
                rmdir("views/img/usuarios/".$_GET["usuario"]);
            }

            $respuesta = ModelUser::mdlDeleteUser($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El usuario ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "usuarios";
                        }
                    });
                </script>';
            }
        }
    }
}