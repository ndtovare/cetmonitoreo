<?php
class ControllerPaymentControl {
    static public function ctrlShowPaymentControl($item, $value) {
        $respuesta = ModelPaymentControl::mdlShowPaymentControl($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertPaymentControl() {
        if(isset($_POST["nuevoTotal"])) {
            $datos = array(
                "total_payment_control" => $_POST["nuevoTotal"],
                "on_account_payment_control" => $_POST["nuevoACuenta"],
                "substraction_payment_control" => $_POST["nuevoResto"],
                "id_payment" => $_POST["nuevoControlPago"]
            );
            
            $respuesta = ModelPaymentControl::mdlInsertPaymentControl($datos);

            if($respuesta == "ok") {
                echo '
                <script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El control de pago ha sido registrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "control-pagos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditPaymentControl() {
        if(isset($_POST["editarTotal"])) {
            $datos = array(
                "total_payment_control" => $_POST["editarTotal"],
                "on_account_payment_control" => $_POST["editarACuenta"],
                "substraction_payment_control" => $_POST["editarResto"],
                "id_payment" => $_POST["editarControlPago"]
            );

            $idControlPago = $_POST["idControlPago"];
            $respuesta = ModelPaymentControl::mdlEditPaymentControl($idControlPago, $datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El control de pago ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "control-pagos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeletePaymentControl() {
        if(isset($_GET["idControlPago"])) {
            $datos = $_GET["idControlPago"];
            $respuesta = ModelPaymentControl::mdlDeletePaymentControl($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El control de pagos ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "control-pagos";
                        }
                    });
                </script>';
            }
        }
    }
}