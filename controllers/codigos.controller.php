<?php
class ControllerCode {
    static public function ctrlValidateCodes() {
        $respuesta = ModelCode::mdlValidateCodes();
        return $respuesta;
    }
    
    static public function ctrlShowCodes($item, $value) {
        $respuesta = ModelCode::mdlShowCodes($item, $value);
        return $respuesta;
    }

    static public function ctrlInsertCode() {
        if(isset($_POST["nuevoCodigo"])) {
            $esValido = ModelCode::mdlValidateCode($_POST["nuevoCodigo"]);
            
            if($esValido == 0) {
                $ruta = "";

                if(isset($_FILES["nuevaFoto"]["tmp_name"])) {
                    list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "views/img/productos/".$_POST["nuevoCodigo"];
                    mkdir($directorio, 0755);
                    
                    if($_FILES["nuevaFoto"]["type"] == "image/jpeg") {
                        $aleatorio = mt_rand(100, 999);
                        $ruta = "views/img/productos/".$_POST["nuevoCodigo"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }

                    if($_FILES["nuevaFoto"]["type"] == "image/png") {
                        $aleatorio = mt_rand(100, 999);
                        $ruta = "views/img/productos/".$_POST["nuevoCodigo"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }

                $datos = array(
                    "name_code" => $_POST["nuevoCodigo"],
                    "description_code" => $_POST["nuevaDescripcion"],
                    "price_code" => $_POST["nuevoPrecio"],
                    "image_code" => $ruta
                );

                $respuesta = ModelCode::mdlInsertCode($datos);

                if($respuesta == "ok") {
                    echo '
                    <script>
                        Swal.fire({
                            icon: "success",
                            title: "¡El código ha sido registrado correctamente!",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        }).then((result) => {
                            if(result.value) {
                                window.location = "codigos";
                            }
                        });
                    </script>';
                }
            } else {
                echo '
                <script>
                    Swal.fire({
                        icon: "warning",
                        title: "¡El código ya está registrado!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    });
                </script>';
            }
        }
    }

    static public function ctrlEditCode() {
        if(isset($_POST["editarCodigo"])) {
            $ruta = $_POST["fotoActual"];

            if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"])) {
                list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);
                $nuevoAncho = 500;
                $nuevoAlto = 500;
                $directorio = "views/img/productos/".$_POST["editarCodigo"];

                if(!empty($_POST["fotoActual"])) {
                    unlink($_POST["fotoActual"]);
                } else {
                    mkdir($directorio, 0755);
                }
                
                if($_FILES["editarFoto"]["type"] == "image/jpeg") {
                    $aleatorio = mt_rand(100, 999);
                    $ruta = "views/img/productos/".$_POST["editarCodigo"]."/".$aleatorio.".jpg";
                    $origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagejpeg($destino, $ruta);
                }

                if($_FILES["editarFoto"]["type"] == "image/png") {
                    $aleatorio = mt_rand(100, 999);
                    $ruta = "views/img/productos/".$_POST["editarCodigo"]."/".$aleatorio.".png";
                    $origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagepng($destino, $ruta);
                }
            }

            $datos = array(
                "name_code" => $_POST["editarCodigo"],
                "description_code" => $_POST["editarDescripcion"],
                "price_code" => $_POST["editarPrecio"],
                "image_code" => $ruta
            );

            $idCodigo = $_POST["idCodigo"];
            echo '<script>console.log('.json_encode($idCodigo).')</script>';
            echo '<script>console.log('.json_encode($datos).')</script>';
            $respuesta = ModelCode::mdlEditCode($idCodigo, $datos);
            echo '<script>console.log('.json_encode($respuesta).')</script>';

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El código ha sido editado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "codigos";
                        }
                    });
                </script>';
            }
        }
    }

    static public function ctrlDeleteCode() {
        if(isset($_GET["idCodigo"])) {
            $datos = $_GET["idCodigo"];

            if($_GET["fotoCodigo"] != "") {
                unlink($_GET["fotoCodigo"]);
                rmdir("views/img/productos/".$_GET["codigo"]);
            }

            $respuesta = ModelCode::mdlDeleteCode($datos);

            if($respuesta == "ok") {
                echo '<script>
                    Swal.fire({
                        icon: "success",
                        title: "¡El código ha sido borrado correctamente!",
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    }).then((result) => {
                        if(result.value) {
                            window.location = "codigos";
                        }
                    });
                </script>';
            }
        }
    }
}