<?php
require_once "conexion.php";

class ModelPayment {
    static public function mdlValidatePayment($identificador) {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS is_valid_payment 
            FROM cet_payments 
            WHERE identifier_payment = '$identificador'"
        );
        
        $stmt->bindParam(":identifier_payment", $identificador, PDO::PARAM_STR);

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlShowPayments($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT CP.id_payment, 
                        CP.identifier_payment,
                        CP.month_payment,
                        CP.method_payment, 
                        CP.number_payment, 
                        CP.date_payment, 
                        CP.amount_payment
                FROM cet_payments CP
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CP.id_payment, 
                        CP.identifier_payment,
                        CP.month_payment,
                        CP.method_payment, 
                        CP.number_payment, 
                        CP.date_payment, 
                        CP.amount_payment
                FROM cet_payments CP "
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertPayment($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_payments(
                identifier_payment, 
                month_payment, 
                method_payment, 
                number_payment,
                date_payment, 
                amount_payment
            ) 
            VALUES(
                :identifier_payment, 
                :month_payment, 
                :method_payment, 
                :number_payment,
                :date_payment, 
                :amount_payment
            )"
        );

        $stmt->bindParam(":identifier_payment", $data["identifier_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":month_payment", $data["month_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":method_payment", $data["method_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":number_payment", $data["number_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":date_payment", $data["date_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":amount_payment", $data["amount_payment"], PDO::PARAM_STR);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditPayment($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_payments 
            SET identifier_payment = :identifier_payment, 
                month_payment = :month_payment, 
                method_payment = :method_payment, 
                date_payment = :date_payment, 
                amount_payment = :amount_payment
            WHERE id_payment = $id"
        );

        $stmt->bindParam(":identifier_payment", $data["identifier_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":month_payment", $data["month_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":method_payment", $data["method_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":date_payment", $data["date_payment"], PDO::PARAM_STR);
        $stmt->bindParam(":amount_payment", $data["amount_payment"], PDO::PARAM_STR);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeletePayment($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_payments
            WHERE id_payment = :id_payment"
        );
        
        $stmt->bindParam(":id_payment", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}