<?php
require_once "conexion.php";

class ModelSim {
    static public function mdlValidateSim($sim) {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS is_valid_sim 
            FROM cet_sims 
            WHERE serial_number_sim = '$sim'"
        );
        
        $stmt->bindParam(":serial_number_sim", $sim, PDO::PARAM_STR);

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlShowSims($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT id_sim, 
                        status_sim, 
                        serial_number_sim, 
                        number_sim, 
                        scheme_sim, 
                        balance_sim, 
                        date_sim, 
                        cutting_day_sim, 
                        id_code, 
                        id_lot, 
                        id_user 
                FROM cet_sims 
                WHERE id_sim = :$item"
            );
            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CS.id_sim, 
                        CS.status_sim, 
                        CS.serial_number_sim, 
                        CS.number_sim, 
                        CS.scheme_sim, 
                        CS.balance_sim, 
                        CS.date_sim, 
                        CS.cutting_day_sim, 
                        CC.id_code, 
                        CC.name_code, 
                        CL.id_lot, 
                        CL.bill_lot, 
                        CU.id_user, 
                        CU.nickname_user 
                FROM cet_sims CS 
                INNER JOIN cet_codes CC ON CS.id_code = CC.id_code 
                INNER JOIN cet_lots CL ON CS.id_lot = CL.id_lot 
                INNER JOIN cet_users CU ON CS.id_user = CU.id_user"
            );
            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertSim($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_sims(
                status_sim, 
                serial_number_sim, 
                number_sim, 
                scheme_sim, 
                balance_sim, 
                date_sim, 
                cutting_day_sim, 
                id_code, 
                id_lot, 
                id_user
            ) 
            VALUES(
                :status_sim, 
                :serial_number_sim, 
                :number_sim, 
                :scheme_sim, 
                :balance_sim, 
                :date_sim, 
                :cutting_day_sim, 
                :id_code, 
                :id_lot, 
                :id_user
            )"
        );

        $stmt->bindParam(":status_sim", $data["status_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":serial_number_sim", $data["serial_number_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":number_sim", $data["number_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":scheme_sim", $data["scheme_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":balance_sim", $data["balance_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":date_sim", $data["date_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":cutting_day_sim", $data["cutting_day_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":id_code", $data["id_code"], PDO::PARAM_INT);
        $stmt->bindParam(":id_lot", $data["id_lot"], PDO::PARAM_INT);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditSim($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_sims 
            SET status_sim = :status_sim, 
                serial_number_sim = :serial_number_sim, 
                number_sim = :number_sim, 
                scheme_sim = :scheme_sim, 
                balance_sim = :balance_sim, 
                date_sim = :date_sim, 
                cutting_day_sim = :cutting_day_sim, 
                id_code = :id_code, 
                id_lot = :id_lot, 
                id_user = :id_user 
            WHERE id_sim = $id"
        );

        $stmt->bindParam(":status_sim", $data["status_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":serial_number_sim", $data["serial_number_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":number_sim", $data["number_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":scheme_sim", $data["scheme_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":balance_sim", $data["balance_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":date_sim", $data["date_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":cutting_day_sim", $data["cutting_day_sim"], PDO::PARAM_STR);
        $stmt->bindParam(":id_code", $data["id_code"], PDO::PARAM_INT);
        $stmt->bindParam(":id_lot", $data["id_lot"], PDO::PARAM_INT);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        echo '<script>console.log('.json_encode($datos).')</script>';
        echo '<script>console.log('.json_encode($id).')</script>';
        echo '<script>console.log('.json_encode($stmt).')</script>';
        
        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteSim($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_sims 
            WHERE id_sim = :id_sim"
        );
        
        $stmt->bindParam(":id_sim", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}