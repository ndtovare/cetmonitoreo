<?php
require_once "conexion.php";

class ModelFleet {
    static public function mdlShowFleets($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT CF.id_fleet, 
                        CF.name_fleet, 
                        CF.address_fleet, 
                        CF.phone_contact_fleet, 
                        CF.name_contact_fleet, 
                        CF.last_name_contact_fleet, 
                        CF.id_business, 
                        CV.id_vehicle, 
                        CV.identifier_private_public_vehicle, 
                        CV.type_vehicle 
                FROM cet_fleets CF 
                INNER JOIN cet_vehicles CV ON CF.id_vehicle = CV.id_vehicle
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CF.id_fleet, 
                        CF.name_fleet, 
                        CF.address_fleet, 
                        CF.phone_contact_fleet, 
                        CF.name_contact_fleet, 
                        CF.last_name_contact_fleet, 
                        CB.id_business, 
                        CB.name_business, 
                        CV.id_vehicle, 
                        CV.identifier_private_public_vehicle, 
                        CV.type_vehicle 
                FROM cet_fleets CF 
                INNER JOIN cet_business CB ON CF.id_business = CB.id_business 
                INNER JOIN cet_vehicles CV ON CF.id_vehicle = CV.id_vehicle"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertFleet($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_fleets(
                name_fleet, 
                address_fleet, 
                phone_contact_fleet, 
                name_contact_fleet, 
                last_name_contact_fleet, 
                id_business, 
                id_vehicle
            ) 
            VALUES(
                :name_fleet, 
                :address_fleet, 
                :phone_contact_fleet, 
                :name_contact_fleet, 
                :last_name_contact_fleet, 
                :id_business, 
                :id_vehicle
            )"
        );

        $stmt->bindParam(":name_fleet", $data["name_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":address_fleet", $data["address_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_contact_fleet", $data["phone_contact_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":name_contact_fleet", $data["name_contact_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":last_name_contact_fleet", $data["last_name_contact_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":id_business", $data["id_business"], PDO::PARAM_INT);
        $stmt->bindParam(":id_vehicle", $data["id_vehicle"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditFleet($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_fleets 
            SET name_fleet = :name_fleet, 
                address_fleet = :address_fleet, 
                phone_contact_fleet = :phone_contact_fleet, 
                name_contact_fleet = :name_contact_fleet, 
                last_name_contact_fleet = :last_name_contact_fleet, 
                id_business = :id_business, 
                id_vehicle = :id_vehicle 
            WHERE id_fleet = $id"
        );

        $stmt->bindParam(":name_fleet", $data["name_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":address_fleet", $data["address_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_contact_fleet", $data["phone_contact_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":name_contact_fleet", $data["name_contact_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":last_name_contact_fleet", $data["last_name_contact_fleet"], PDO::PARAM_STR);
        $stmt->bindParam(":id_business", $data["id_business"], PDO::PARAM_INT);
        $stmt->bindParam(":id_vehicle", $data["id_vehicle"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteFleet($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_fleets 
            WHERE id_fleet = :id_fleet"
        );
        
        $stmt->bindParam(":id_fleet", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}