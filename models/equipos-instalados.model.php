<?php
require_once "conexion.php";

class ModelInstalledEquipment {
    static public function mdlShowInstalledEquipment($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT CIE.id_installed_equipment,
                        CIE.description_installed_equipment,
                        CIE.comments_installed_equipment,
                        CIE.installation_date_installed_equipment,
                        CL.id_lot,
                        CL.bill_lot,
                        CC.id_code,
                        CC.name_code,
                        CC.description_code,
                        CC.image_code,
                        CV.id_vehicle,
                        CV.identifier_private_public_vehicle,
                        CV.type_vehicle,
                        CU.id_user,
                        CU.nickname_user
                FROM cet_installed_equipment CIE 
                INNER JOIN cet_lots CL on CIE.id_lot = CL.id_lot 
                INNER JOIN cet_codes CC ON CIE.id_code = CC.id_code 
                INNER JOIN cet_vehicles CV ON CIE.id_vehicle = CV.id_vehicle
                INNER JOIN cet_users CU ON CIE.id_user = CU.id_user
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CIE.id_installed_equipment,
                        CIE.description_installed_equipment,
                        CIE.comments_installed_equipment,
                        CIE.installation_date_installed_equipment,
                        CL.id_lot,
                        CL.bill_lot,
                        CC.id_code,
                        CC.name_code,
                        CC.description_code,
                        CC.image_code,
                        CV.id_vehicle,
                        CV.identifier_private_public_vehicle,
                        CU.id_user,
                        CU.nickname_user
                FROM cet_installed_equipment CIE 
                INNER JOIN cet_lots CL on CIE.id_lot = CL.id_lot 
                INNER JOIN cet_codes CC ON CIE.id_code = CC.id_code 
                INNER JOIN cet_vehicles CV ON CIE.id_vehicle = CV.id_vehicle
                INNER JOIN cet_users CU ON CIE.id_user = CU.id_user"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertInstalledEquipment($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_installed_equipment(
                description_installed_equipment, 
                comments_installed_equipment,
                installation_date_installed_equipment,
                id_lot, 
                id_code, 
                id_vehicle,
                id_user
            ) 
            VALUES(
                :description_installed_equipment, 
                :comments_installed_equipment,
                :installation_date_installed_equipment,
                :id_lot, 
                :id_code, 
                :id_vehicle,
                :id_user
            )"
        );

        $stmt->bindParam(":description_installed_equipment", $data["description_installed_equipment"], PDO::PARAM_STR);
        $stmt->bindParam(":comments_installed_equipment", $data["comments_installed_equipment"], PDO::PARAM_STR);
        $stmt->bindParam(":installation_date_installed_equipment", $data["installation_date_installed_equipment"], PDO::PARAM_STR);
        $stmt->bindParam(":id_lot", $data["id_lot"], PDO::PARAM_INT);
        $stmt->bindParam(":id_code", $data["id_code"], PDO::PARAM_INT);
        $stmt->bindParam(":id_vehicle", $data["id_vehicle"], PDO::PARAM_INT);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            $datosProducto = array(
                "status_product" => "Instalado",
                "serial_number_product" => $data["serial_number_product"],
                "id_code" => $data["id_code"],
                "id_lot" => $data["id_lot"],
                "id_user" => $_SESSION["id"]
            );

            $idProducto = $data["id_product"];
            $respuesta = ModelProduct::mdlEditProduct($idProducto, $datosProducto);

            if($respuesta == "ok") {
                return "ok";
            } else {
                return "error";
            }

            
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditInstalledEquipment($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_installed_equipment 
            SET description_installed_equipment = :description_installed_equipment, 
                comments_installed_equipment = :comments_installed_equipment, 
                installation_date_installed_equipment = :installation_date_installed_equipment, 
                id_lot = :id_lot, 
                id_code = :id_code, 
                id_vehicle = :id_vehicle,
                id_user = :id_user 
            WHERE id_installed_equipment = $id"
        );

        $stmt->bindParam(":description_installed_equipment", $data["description_installed_equipment"], PDO::PARAM_STR);
        $stmt->bindParam(":comments_installed_equipment", $data["comments_installed_equipment"], PDO::PARAM_STR);
        $stmt->bindParam(":installation_date_installed_equipment", $data["installation_date_installed_equipment"], PDO::PARAM_STR);
        $stmt->bindParam(":id_lot", $data["id_lot"], PDO::PARAM_INT);
        $stmt->bindParam(":id_code", $data["id_code"], PDO::PARAM_INT);
        $stmt->bindParam(":id_vehicle", $data["id_vehicle"], PDO::PARAM_INT);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteInstalledEquipment($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_installed_equipment 
            WHERE id_installed_equipment = :id_installed_equipment"
        );
        
        $stmt->bindParam(":id_installed_equipment", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}