<?php
require_once "conexion.php";

class ModelPrivateVehicle {
    static public function mdlShowPrivateVehicles($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT * 
                FROM cet_vehicles
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CV.id_vehicle, 
                        CV.imei_private_public_vehicle,
                        CV.identifier_private_public_vehicle, 
                        CV.brand_private_public_vehicle, 
                        CV.model_private_public_vehicle, 
                        CV.year_private_public_vehicle, 
                        CV.vehicular_series_private_public_vehicle, 
                        CV.plate_number_private_public_vehicle, 
                        CV.class_private_public_vehicle, 
                        CU.id_user, 
                        CU.nickname_user,
                        CO.id_owner,
                        CONCAT(CO.name_owner, ' ' , CO.last_name_owner) AS owner_vehicle,
                        CD.id_driver,
                        CONCAT(CD.name_driver, ' ' , CD.last_name_driver) AS driver_vehicle
                FROM cet_vehicles CV
                INNER JOIN cet_users CU on CV.id_user = CU.id_user 
                INNER JOIN cet_owners CO ON CV.id_owner = CO.id_owner 
                INNER JOIN cet_drivers CD ON CV.id_driver = CD.id_driver
                WHERE CV.type_vehicle = 'Vehículo privado'"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditPrivateVehicle($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_vehicles 
            SET imei_private_public_vehicle = :imei_private_public_vehicle, 
                identifier_private_public_vehicle = :identifier_private_public_vehicle, 
                brand_private_public_vehicle = :brand_private_public_vehicle,
                model_private_public_vehicle = :model_private_public_vehicle,
                year_private_public_vehicle = :year_private_public_vehicle,
                vehicular_series_private_public_vehicle = :vehicular_series_private_public_vehicle,
                plate_number_private_public_vehicle = :plate_number_private_public_vehicle,
                class_private_public_vehicle = :class_private_public_vehicle,
                id_user = :id_user, 
                id_owner = :id_owner, 
                id_driver = :id_driver 
            WHERE id_vehicle = $id"
        );

        $stmt->bindParam(":imei_private_public_vehicle", $data["imei_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":identifier_private_public_vehicle", $data["identifier_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":brand_private_public_vehicle", $data["brand_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":model_private_public_vehicle", $data["model_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":year_private_public_vehicle", $data["year_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":vehicular_series_private_public_vehicle", $data["vehicular_series_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":plate_number_private_public_vehicle", $data["plate_number_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":class_private_public_vehicle", $data["class_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);
        $stmt->bindParam(":id_owner", $data["id_owner"], PDO::PARAM_INT);
        $stmt->bindParam(":id_driver", $data["id_driver"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeletePrivateVehicle($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_vehicles 
            WHERE id_vehicle = :id_vehicle"
        );
        
        $stmt->bindParam(":id_vehicle", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}