<?php
require_once "conexion.php";

class ModelCompany {
    static public function mdlValidateCompany($acronimo) {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS is_valid_business 
            FROM cet_business 
            WHERE 'acronym_business' = '$acronimo'"
        );
        
        $stmt->bindParam(":acronym_business", $acronimo, PDO::PARAM_STR);

        $stmt->execute();
        echo '<script>console.log('.json_encode($stmt->fetchColumn()).')</script>';
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlShowCompanies($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT *
                FROM cet_business
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CB.id_business, 
                        CB.name_business, 
                        CB.acronym_business, 
                        CB.phone_business, 
                        CB.representative_email_business, 
                        CB.address_business, 
                        CB.email_business, 
                        CB.representative_name_business, 
                        CB.representative_last_name_business, 
                        CB.representative_phone_business, 
                        CB.reference_name_business, 
                        CB.reference_last_name_business, 
                        CB.reference_phone_business,
                        CU.id_user, 
                        CU.nickname_user
                FROM cet_business CB 
                INNER JOIN cet_users CU on CB.id_user = CU.id_user"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertCompany($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_business(
                name_business, 
                acronym_business, 
                phone_business, 
                representative_email_business, 
                address_business, 
                email_business, 
                representative_name_business, 
                representative_last_name_business, 
                representative_phone_business, 
                reference_name_business, 
                reference_last_name_business, 
                reference_phone_business, 
                id_user
            ) 
            VALUES(
                :name_business, 
                :acronym_business, 
                :phone_business, 
                :representative_email_business, 
                :address_business, 
                :email_business, 
                :representative_name_business, 
                :representative_last_name_business, 
                :representative_phone_business, 
                :reference_name_business, 
                :reference_last_name_business, 
                :reference_phone_business, 
                :id_user
            )"
        );

        $stmt->bindParam(":name_business", $data["name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":acronym_business", $data["acronym_business"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_business", $data["phone_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_email_business", $data["representative_email_business"], PDO::PARAM_STR);
        $stmt->bindParam(":address_business", $data["address_business"], PDO::PARAM_STR);
        $stmt->bindParam(":email_business", $data["email_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_name_business", $data["representative_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_last_name_business", $data["representative_last_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_phone_business", $data["representative_phone_business"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_name_business", $data["reference_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_last_name_business", $data["reference_last_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_phone_business", $data["reference_phone_business"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditCompany($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_business 
            SET name_business = :name_business, 
                acronym_business = :acronym_business, 
                phone_business = :phone_business, 
                representative_email_business = :representative_email_business, 
                address_business = :address_business, 
                email_business = :email_business, 
                representative_name_business = :representative_name_business, 
                representative_last_name_business = :representative_last_name_business, 
                representative_phone_business = :representative_phone_business, 
                reference_name_business = :reference_name_business, 
                reference_last_name_business = :reference_last_name_business, 
                reference_phone_business = :reference_phone_business,
                id_user = :id_user
            WHERE id_business = $id"
        );

        $stmt->bindParam(":name_business", $data["name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":acronym_business", $data["acronym_business"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_business", $data["phone_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_email_business", $data["representative_email_business"], PDO::PARAM_STR);
        $stmt->bindParam(":address_business", $data["address_business"], PDO::PARAM_STR);
        $stmt->bindParam(":email_business", $data["email_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_name_business", $data["representative_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_last_name_business", $data["representative_last_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":representative_phone_business", $data["representative_phone_business"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_name_business", $data["reference_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_last_name_business", $data["reference_last_name_business"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_phone_business", $data["reference_phone_business"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteCompany($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_business 
            WHERE id_business = :id_business"
        );
        
        $stmt->bindParam(":id_business", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}