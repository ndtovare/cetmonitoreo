<?php
require_once "conexion.php";

class ModelRevision {
    static public function mdlShowRevisions($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT *
                FROM cet_revisions
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CR.id_revision, 
                        CR.identifier_revision, 
                        CR.comments_revision, 
                        CR.date_revision, 
                        CR.price_revision, 
                        CC.name_code 
                FROM cet_revisions CR 
                INNER JOIN cet_installed_equipment CIE 
                ON CR.id_installed_equipment = CIE.id_installed_equipment 
                INNER JOIN cet_codes CC 
                ON CIE.id_code = CC.id_code"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertRevision($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_revisions(
                identifier_revision,
                comments_revision, 
                date_revision, 
                price_revision, 
                id_installed_equipment
            ) 
            VALUES(
                :identifier_revision,
                :comments_revision, 
                :date_revision, 
                :price_revision, 
                :id_installed_equipment
            )"
        );

        $stmt->bindParam(":identifier_revision", $data["identifier_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":comments_revision", $data["comments_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":date_revision", $data["date_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":price_revision", $data["price_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":id_installed_equipment", $data["id_installed_equipment"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditRevision($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_revisions 
            SET identifier_revision = :identifier_revision, 
                comments_revision = :comments_revision, 
                date_revision = :date_revision, 
                price_revision = :price_revision, 
                id_installed_equipment = :id_installed_equipment 
            WHERE id_revision = $id"
        );

        $stmt->bindParam(":identifier_revision", $data["identifier_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":comments_revision", $data["comments_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":date_revision", $data["date_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":price_revision", $data["price_revision"], PDO::PARAM_STR);
        $stmt->bindParam(":id_installed_equipment", $data["id_installed_equipment"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteRevision($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_revisions 
            WHERE id_revision = :id_revision"
        );
        
        $stmt->bindParam(":id_revision", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}