<?php
require_once "conexion.php";

class ModelPaymentControl {
    static public function mdlShowPaymentControl($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT * 
                FROM cet_payment_control
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CPC.id_payment_control, 
                        CPC.total_payment_control,
                        CPC.on_account_payment_control, 
                        CPC.substraction_payment_control, 
                        CP.id_payment,
                        CP.identifier_payment
                FROM cet_payment_control CPC 
                INNER JOIN cet_payments CP on CPC.id_payment = CP.id_payment"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertPaymentControl($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_payment_control(
                total_payment_control, 
                on_account_payment_control, 
                substraction_payment_control, 
                id_payment
            ) 
            VALUES(
                :total_payment_control, 
                :on_account_payment_control, 
                :substraction_payment_control, 
                :id_payment
            )"
        );

        $stmt->bindParam(":total_payment_control", $data["total_payment_control"], PDO::PARAM_STR);
        $stmt->bindParam(":on_account_payment_control", $data["on_account_payment_control"], PDO::PARAM_STR);
        $stmt->bindParam(":substraction_payment_control", $data["substraction_payment_control"], PDO::PARAM_STR);
        $stmt->bindParam(":id_payment", $data["id_payment"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditPaymentControl($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_payment_control 
            SET total_payment_control = :total_payment_control, 
                on_account_payment_control = :on_account_payment_control, 
                substraction_payment_control = :substraction_payment_control, 
                id_payment = :id_payment
            WHERE id_payment_control = $id"
        );

        $stmt->bindParam(":total_payment_control", $data["total_payment_control"], PDO::PARAM_STR);
        $stmt->bindParam(":on_account_payment_control", $data["on_account_payment_control"], PDO::PARAM_STR);
        $stmt->bindParam(":substraction_payment_control", $data["substraction_payment_control"], PDO::PARAM_STR);
        $stmt->bindParam(":id_payment", $data["id_payment"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeletePaymentControl($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_payment_control 
            WHERE id_payment_control = :id_payment_control"
        );
        
        $stmt->bindParam(":id_payment_control", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}