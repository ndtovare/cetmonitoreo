<?php
require_once "conexion.php";

class ModelLot {
    static public function mdlValidateLots() {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS quantity_lot 
            FROM cet_lots"
        );

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlShowLots($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT CL.id_lot, 
                        CL.bill_lot, 
                        CL.date_admission_lot, 
                        CL.warranty_date_lot, 
                        CU.id_user, 
                        CU.nickname_user 
                FROM cet_lots CL 
                INNER JOIN cet_users CU ON CL.id_lot = :$item AND CL.id_user = CU.id_user"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CL.id_lot, 
                        CL.bill_lot, 
                        CL.date_admission_lot, 
                        CL.warranty_date_lot, 
                        CU.id_user, 
                        CU.nickname_user 
                FROM cet_lots CL 
                INNER JOIN cet_users CU ON CL.id_user = CU.id_user"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertLot($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_lots(
                bill_lot, 
                date_admission_lot, 
                warranty_date_lot, 
                id_user
            ) 
            VALUES(
                :bill_lot, 
                :date_admission_lot, 
                :warranty_date_lot, 
                :id_user
            )"
        );

        $stmt->bindParam(":bill_lot", $data["bill_lot"], PDO::PARAM_STR);
        $stmt->bindParam(":date_admission_lot", $data["date_admission_lot"], PDO::PARAM_STR);
        $stmt->bindParam(":warranty_date_lot", $data["warranty_date_lot"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditLot($id, $datos) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_lots 
            SET bill_lot = :bill_lot, 
                date_admission_lot = :date_admission_lot, 
                warranty_date_lot = :warranty_date_lot, 
                id_user = :id_user 
            WHERE id_lot = $id"
        );

        $stmt->bindParam(":bill_lot", $datos["bill_lot"], PDO::PARAM_STR);
        $stmt->bindParam(":date_admission_lot", $datos["date_admission_lot"], PDO::PARAM_STR);
        $stmt->bindParam(":warranty_date_lot", $datos["warranty_date_lot"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $datos["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteLot($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_lots 
            WHERE id_lot = :id_lot"
        );
        
        $stmt->bindParam(":id_lot", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}