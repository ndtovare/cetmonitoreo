<?php
require_once "conexion.php";

class ModelUser {
    static public function mdlLoginUser($value) {
        $stmt = Connection::connect()->prepare(
            "SELECT *
            FROM cet_users
            WHERE nickname_user = :nickname_user"
        );

        $stmt->bindParam(":nickname_user", $value, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
    }

    static public function mdlShowUsers($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT *
                FROM cet_users
                WHERE $item = $value"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT id_user, 
                        nickname_user, 
                        password_user, 
                        name_user, 
                        last_name_user,
                        phone_user, 
                        reference_name_user, 
                        reference_phone_user, 
                        address_user, 
                        email_user, 
                        image_user, 
                        role_user
                FROM cet_users"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertUser($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_users(
                nickname_user, 
                password_user, 
                name_user, 
                last_name_user,
                phone_user, 
                reference_name_user, 
                reference_phone_user, 
                address_user, 
                email_user, 
                image_user, 
                role_user
            ) 
            VALUES(
                :nickname_user, 
                :password_user, 
                :name_user, 
                :last_name_user,
                :phone_user, 
                :reference_name_user, 
                :reference_phone_user, 
                :address_user, 
                :email_user, 
                :image_user, 
                :role_user
            )"
        );

        $stmt->bindParam(":nickname_user", $data["nickname_user"], PDO::PARAM_STR);
        $stmt->bindParam(":password_user", $data["password_user"], PDO::PARAM_STR);
        $stmt->bindParam(":name_user", $data["name_user"], PDO::PARAM_STR);
        $stmt->bindParam(":last_name_user", $data["last_name_user"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_user", $data["phone_user"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_name_user", $data["reference_name_user"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_phone_user", $data["reference_phone_user"], PDO::PARAM_STR);
        $stmt->bindParam(":address_user", $data["address_user"], PDO::PARAM_STR);
        $stmt->bindParam(":email_user", $data["email_user"], PDO::PARAM_STR);
        $stmt->bindParam(":image_user", $data["image_user"], PDO::PARAM_STR);
        $stmt->bindParam(":role_user", $data["role_user"], PDO::PARAM_STR);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditUser($id, $datos) {

        $stmt = Connection::connect()->prepare(
            "UPDATE cet_users 
            SET nickname_user = :nickname_user, 
                password_user = :password_user, 
                name_user = :name_user, 
                last_name_user = :last_name_user, 
                phone_user = :phone_user, 
                reference_name_user = :reference_name_user, 
                reference_phone_user = :reference_phone_user, 
                address_user = :address_user, 
                email_user = :email_user, 
                image_user = :image_user, 
                role_user = :role_user 
            WHERE id_user = $id"
        );

        $stmt->bindParam(":nickname_user", $datos["nickname_user"], PDO::PARAM_STR);
        $stmt->bindParam(":password_user", $datos["password_user"], PDO::PARAM_STR);
        $stmt->bindParam(":name_user", $datos["name_user"], PDO::PARAM_STR);
        $stmt->bindParam(":last_name_user", $datos["last_name_user"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_user", $datos["phone_user"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_name_user", $datos["reference_name_user"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_phone_user", $datos["reference_phone_user"], PDO::PARAM_STR);
        $stmt->bindParam(":address_user", $datos["address_user"], PDO::PARAM_STR);
        $stmt->bindParam(":email_user", $datos["email_user"], PDO::PARAM_STR);
        $stmt->bindParam(":image_user", $datos["image_user"], PDO::PARAM_STR);
        $stmt->bindParam(":role_user", $datos["role_user"], PDO::PARAM_STR);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteUser($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_users 
            WHERE id_user = :id_user"
        );
        
        $stmt->bindParam(":id_user", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}