<?php
require_once "conexion.php";

class ModelGenericUser {
    static public function mdlShowGenericUsers($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT CGU.id_generic_user, 
                        CGU.password_generic_user,  
                        CV.id_vehicle, 
                        CV.identifier_private_public_vehicle, 
                        CV.type_vehicle 
                FROM cet_generic_users CGU
                INNER JOIN cet_vehicles CV ON CGU.id_vehicle = CV.id_vehicle
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CGU.id_generic_user, 
                        CGU.password_generic_user,  
                        CV.id_vehicle, 
                        CV.identifier_private_public_vehicle, 
                        CV.type_vehicle 
                FROM cet_generic_users CGU 
                INNER JOIN cet_vehicles CV ON CGU.id_vehicle = CV.id_vehicle"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertGenericUser($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_generic_users(
                password_generic_user,
                id_vehicle
            ) 
            VALUES(
                :password_generic_user,
                :id_vehicle
            )"
        );

        $stmt->bindParam(":password_generic_user", $data["password_generic_user"], PDO::PARAM_STR);
        $stmt->bindParam(":id_vehicle", $data["id_vehicle"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditGenericUser($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_generic_users 
            SET password_generic_user = :password_generic_user, 
                id_vehicle = :id_vehicle 
            WHERE id_generic_user = $id"
        );

        $stmt->bindParam(":password_generic_user", $data["password_generic_user"], PDO::PARAM_STR);
        $stmt->bindParam(":id_vehicle", $data["id_vehicle"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteGenericUser($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_generic_users 
            WHERE id_generic_user = :id_generic_user"
        );
        
        $stmt->bindParam(":id_generic_user", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}