<?php
require_once "conexion.php";

class ModelCode {
    static public function mdlValidateCodes() {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS quantity_code 
            FROM cet_codes"
        );

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlValidateCode($codigo) {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS is_valid_code 
            FROM cet_codes 
            WHERE name_code = '$codigo'"
        );
        
        $stmt->bindParam(":name_code", $codigo, PDO::PARAM_STR);

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlShowCodes($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT * 
                FROM cet_codes 
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT * 
                FROM cet_codes"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertCode($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_codes(
                name_code, 
                description_code, 
                image_code, 
                price_code
            ) 
            VALUES(
                :name_code, 
                :description_code, 
                :image_code, 
                :price_code
            )"
        );

        $stmt->bindParam(":name_code", $data["name_code"], PDO::PARAM_STR);
        $stmt->bindParam(":description_code", $data["description_code"], PDO::PARAM_STR);
        $stmt->bindParam(":image_code", $data["image_code"], PDO::PARAM_STR);
        $stmt->bindParam(":price_code", $data["price_code"], PDO::PARAM_STR);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditCode($id, $datos) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_codes 
            SET name_code = :name_code, 
                description_code = :description_code, 
                image_code = :image_code, 
                price_code = :price_code 
            WHERE id_code = $id"
        );

        $stmt->bindParam(":name_code", $datos["name_code"], PDO::PARAM_STR);
        $stmt->bindParam(":description_code", $datos["description_code"], PDO::PARAM_STR);
        $stmt->bindParam(":image_code", $datos["image_code"], PDO::PARAM_STR);
        $stmt->bindParam(":price_code", $datos["price_code"], PDO::PARAM_STR);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteCode($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_codes 
            WHERE id_code = :id_code"
        );
        
        $stmt->bindParam(":id_code", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}