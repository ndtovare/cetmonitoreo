<?php
require_once "conexion.php";

class ModelDriver {
    static public function mdlShowDrivers($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT * 
                FROM cet_clients
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CC.id_client, 
                        CC.name_owner_driver_client,
                        CC.last_name_owner_driver_client,
                        CC.phone_owner_driver_client, 
                        CC.identification_owner_driver_client, 
                        CC.identification_number_owner_driver_client, 
                        CC.address_owner_driver_client, 
                        CC.reference_name_owner_driver_client,
                        CC.reference_last_name_owner_driver_client,
                        CC.reference_phone_owner_driver_client,
                        CC.type_client, 
                        CU.nickname_user
                FROM cet_clients CC 
                INNER JOIN cet_users CU ON CC.id_user = CU.id_user
                WHERE CC.type_client = 'Chofer'
                OR CC.type_client = 'Propietario - chofer'"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditDriver($id, $data) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_clients 
            SET name_owner_driver_client = :name_owner_driver_client, 
                last_name_owner_driver_client = :last_name_owner_driver_client, 
                phone_owner_driver_client = :phone_owner_driver_client, 
                identification_owner_driver_client = :identification_owner_driver_client, 
                identification_number_owner_driver_client = :identification_number_owner_driver_client, 
                address_owner_driver_client = :address_owner_driver_client, 
                reference_name_owner_driver_client = :reference_name_owner_driver_client, 
                reference_last_name_owner_driver_client = :reference_last_name_owner_driver_client, 
                reference_phone_owner_driver_client = :reference_phone_owner_driver_client, 
                type_client = :type_client, 
                id_user = :id_user 
            WHERE id_client = $id"
        );

        $stmt->bindParam(":name_owner_driver_client", $datos["name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":last_name_owner_driver_client", $datos["last_name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_owner_driver_client", $datos["phone_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":identification_owner_driver_client", $datos["identification_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":identification_number_owner_driver_client", $datos["identification_number_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":address_owner_driver_client", $datos["address_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_name_owner_driver_client", $datos["reference_name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_last_name_owner_driver_client", $data["reference_last_name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_phone_owner_driver_client", $datos["reference_phone_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":type_client", $datos["type_client"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $datos["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteDriver($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_clients 
            WHERE id_client = :id_client"
        );
        
        $stmt->bindParam(":id_client", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}