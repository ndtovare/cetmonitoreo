<?php
require_once "conexion.php";

class ModelVehicle {
    static public function mdlValidateVehicle($campo, $valor) {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS is_valid_vehicle 
            FROM cet_vehicles 
            WHERE $campo = '$valor'"
        );
        
        $stmt->bindParam(":".$campo, $valor, PDO::PARAM_STR);

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertVehicle($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_vehicles(
                imei_private_public_vehicle,
                identifier_private_public_vehicle,
                brand_private_public_vehicle,
                model_private_public_vehicle,
                year_private_public_vehicle,
                vehicular_series_private_public_vehicle,
                plate_number_private_public_vehicle,
                class_private_public_vehicle,
                economic_number_public_vehicle,
                type_vehicle,
                id_user,
                id_owner,
                id_driver,
                id_business
            ) 
            VALUES(
                :imei_private_public_vehicle,
                :identifier_private_public_vehicle,
                :brand_private_public_vehicle,
                :model_private_public_vehicle,
                :year_private_public_vehicle,
                :vehicular_series_private_public_vehicle,
                :plate_number_private_public_vehicle,
                :class_private_public_vehicle,
                :economic_number_public_vehicle,
                :type_vehicle,
                :id_user,
                :id_owner,
                :id_driver,
                :id_business
            )"
        );

        $stmt->bindParam(":imei_private_public_vehicle", $data["imei_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":identifier_private_public_vehicle", $data["identifier_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":brand_private_public_vehicle", $data["brand_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":model_private_public_vehicle", $data["model_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":year_private_public_vehicle", $data["year_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":vehicular_series_private_public_vehicle", $data["vehicular_series_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":plate_number_private_public_vehicle", $data["plate_number_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":class_private_public_vehicle", $data["class_private_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":economic_number_public_vehicle", $data["economic_number_public_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":type_vehicle", $data["type_vehicle"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);
        $stmt->bindParam(":id_owner", $data["id_owner"], PDO::PARAM_INT);
        $stmt->bindParam(":id_driver", $data["id_driver"], PDO::PARAM_INT);
        $stmt->bindParam(":id_business", $data["id_business"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}