<?php
require_once "conexion.php";

class ModelProduct {
    static public function mdlValidateProduct($serie, $codigo) {
        $stmt = Connection::connect()->prepare(
            "SELECT COUNT(*) AS is_valid_product 
            FROM cet_products 
            WHERE serial_number_product = '$serie'
            AND id_code = '$codigo'"
        );
        
        $stmt->bindParam(":serial_number_product", $serie, PDO::PARAM_STR);
        $stmt->bindParam(":id_code", $codigo, PDO::PARAM_INT);

        $stmt->execute();
        return $stmt->fetchColumn();

        $stmt->close();
        $stmt = null;
    }

    static public function mdlShowProducts($item, $value, $isInstalled) {
        if($item != null && $isInstalled != 1) {
            $stmt = Connection::connect()->prepare(
                "SELECT *
                FROM cet_products CP, 
                     cet_codes CC, 
                     cet_lots CL, 
                     cet_users CU 
                WHERE $item = :$item 
                AND CP.id_code = CC.id_code 
                AND CP.id_lot = CL.id_lot 
                AND CP.id_user = CU.id_user"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else if($isInstalled != 1) {
            $stmt = Connection::connect()->prepare(
                "SELECT CP.id_product, 
                        CP.id_code,
                        CC.name_code, 
                        CC.image_code, 
                        CP.id_lot, 
                        CL.bill_lot, 
                        CL.date_admission_lot, 
                        CP.status_product, 
                        CP.serial_number_product, 
                        CU.id_user, 
                        CU.nickname_user 
                FROM cet_products CP 
                INNER JOIN cet_codes CC on CP.id_code = CC.id_code 
                INNER JOIN cet_lots CL ON CP.id_lot = CL.id_lot 
                INNER JOIN cet_users CU ON CP.id_user = CU.id_user"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CP.id_product, 
                        CP.id_code,
                        CC.name_code, 
                        CC.description_code, 
                        CC.image_code, 
                        CP.id_lot, 
                        CL.bill_lot, 
                        CL.date_admission_lot, 
                        CP.status_product, 
                        CP.serial_number_product, 
                        CU.id_user, 
                        CU.nickname_user 
                FROM cet_products CP 
                INNER JOIN cet_codes CC on CP.id_code = CC.id_code 
                INNER JOIN cet_lots CL ON CP.id_lot = CL.id_lot 
                INNER JOIN cet_users CU ON CP.id_user = CU.id_user
                WHERE CP.status_product <> 'Instalado'"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertProduct($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_products(
                status_product, 
                serial_number_product, 
                id_code, 
                id_lot, 
                id_user
            ) 
            VALUES(
                :status_product, 
                :serial_number_product, 
                :id_code, 
                :id_lot, 
                :id_user
            )"
        );

        $stmt->bindParam(":status_product", $data["status_product"], PDO::PARAM_STR);
        $stmt->bindParam(":serial_number_product", $data["serial_number_product"], PDO::PARAM_STR);
        $stmt->bindParam(":id_code", $data["id_code"], PDO::PARAM_INT);
        $stmt->bindParam(":id_lot", $data["id_lot"], PDO::PARAM_INT);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlEditProduct($id, $datos) {
        $stmt = Connection::connect()->prepare(
            "UPDATE cet_products 
            SET status_product = :status_product, 
                serial_number_product = :serial_number_product, 
                id_code = :id_code, 
                id_lot = :id_lot, 
                id_user = :id_user 
            WHERE id_product = $id"
        );

        $stmt->bindParam(":status_product", $datos["status_product"], PDO::PARAM_STR);
        $stmt->bindParam(":serial_number_product", $datos["serial_number_product"], PDO::PARAM_STR);
        $stmt->bindParam(":id_code", $datos["id_code"], PDO::PARAM_INT);
        $stmt->bindParam(":id_lot", $datos["id_lot"], PDO::PARAM_INT);
        $stmt->bindParam(":id_user", $datos["id_user"], PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlDeleteProduct($datos) {
        $stmt = Connection::connect()->prepare(
            "DELETE FROM cet_products 
            WHERE id_product = :id_product"
        );
        
        $stmt->bindParam(":id_product", $datos, PDO::PARAM_INT);

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}