<?php
require_once "conexion.php";

class ModelClient {
    static public function mdlShowDrivers($item, $value) {
        if($item != null) {
            $stmt = Connection::connect()->prepare(
                "SELECT * 
                FROM cet_clients
                WHERE $item = :$item"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        } else {
            $stmt = Connection::connect()->prepare(
                "SELECT CC.id_client, 
                        CC.name_owner_driver_client,
                        CC.last_name_owner_driver_client,
                        CC.phone_owner_driver_client, 
                        CC.identification_owner_driver_client, 
                        CC.identification_number_owner_driver_client, 
                        CC.address_owner_driver_client, 
                        CC.reference_name_owner_driver_client,
                        CC.reference_last_name_owner_driver_client,
                        CC.reference_phone_owner_driver_client,
                        CC.type_client, 
                        CU.nickname_user
                FROM cet_clients CC 
                INNER JOIN cet_users CU ON CC.id_user = CU.id_user
                WHERE CC.type_client = 'Propietario'
                OR CC.type_client = 'Propietario - chofer'"
            );

            $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        $stmt->close();
        $stmt = null;
    }

    static public function mdlInsertClient($data) {
        $stmt = Connection::connect()->prepare(
            "INSERT INTO cet_clients(
                name_owner_driver_client, 
                last_name_owner_driver_client,
                phone_owner_driver_client, 
                landline_owner_client, 
                identification_owner_driver_client, 
                identification_number_owner_driver_client, 
                address_owner_driver_client, 
                email_owner_client, 
                reference_name_owner_driver_client, 
                reference_last_name_owner_driver_client, 
                reference_phone_owner_driver_client,
                type_client, 
                id_user
            ) 
            VALUES(
                :name_owner_driver_client, 
                :last_name_owner_driver_client,
                :phone_owner_driver_client, 
                :landline_owner_client, 
                :identification_owner_driver_client, 
                :identification_number_owner_driver_client, 
                :address_owner_driver_client, 
                :email_owner_client, 
                :reference_name_owner_driver_client, 
                :reference_last_name_owner_driver_client, 
                :reference_phone_owner_driver_client,
                :type_client, 
                :id_user
            )"
        );
        
        echo '<script>console.log('.json_encode($stmt).')</script>';

        $stmt->bindParam(":name_owner_driver_client", $data["name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":last_name_owner_driver_client", $data["last_name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":phone_owner_driver_client", $data["phone_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":landline_owner_client", $data["landline_owner_client"], PDO::PARAM_STR);
        $stmt->bindParam(":identification_owner_driver_client", $data["identification_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":identification_number_owner_driver_client", $data["identification_number_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":address_owner_driver_client", $data["address_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":email_owner_client", $data["email_owner_client"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_name_owner_driver_client", $data["reference_name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_last_name_owner_driver_client", $data["reference_last_name_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":reference_phone_owner_driver_client", $data["reference_phone_owner_driver_client"], PDO::PARAM_STR);
        $stmt->bindParam(":type_client", $data["type_client"], PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $data["id_user"], PDO::PARAM_INT);

        echo '<script>console.log('.json_encode($stmt->execute()).')</script>';

        if($stmt->execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt = null;
    }
}