<?php
require_once "../controllers/revisiones.controller.php";
require_once "../models/revisiones.model.php";

class AjaxRevisions {
    public $idRevision;

    public function ajaxEditRevision() {
        $item = "id_revision";
        $valor = $this->idRevision;
        $respuesta = ControllerRevision::ctrlShowRevisions($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idRevision"])) {
    $editar = new AjaxRevisions();
    $editar->idRevision = $_POST["idRevision"];
    $editar->ajaxEditRevision();
}