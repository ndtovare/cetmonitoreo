<?php
require_once "../controllers/usuarios-genericos.controller.php";
require_once "../models/usuarios-genericos.model.php";

class AjaxGenericUsers {
    public $idUsuarioGenerico;
    
    public function ajaxEditGenericUser() {
        $item = "id_generic_user";
        $valor = $this->idUsuarioGenerico;
        $respuesta = ControllerGenericUser::ctrlShowGenericUsers($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idUsuarioGenerico"])) {
    $editar = new AjaxGenericUsers();
    $editar->idUsuarioGenerico = $_POST["idUsuarioGenerico"];
    $editar->ajaxEditGenericUser();
}
