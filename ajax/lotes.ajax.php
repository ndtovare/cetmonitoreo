<?php
require_once "../controllers/lotes.controller.php";
require_once "../models/lotes.model.php";

class AjaxLots {
    public $idLote;

    public function ajaxEditLot() {
        $item = "id_lot";
        $valor = $this->idLote;
        $respuesta = ControllerLot::ctrlShowLots($item, $valor);
        echo json_encode($respuesta);
    }

    public function ajaxValidateLots() {
        $respuesta = ControllerLot::ctrlValidateLots();
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idLote"])) {
    $editar = new AjaxLots();
    $editar->idLote = $_POST["idLote"];
    $editar->ajaxEditLot();
}

if(isset($_POST["validaLote"])) {
    $validar = new AjaxLots();
    $validar->ajaxValidateLots();
}