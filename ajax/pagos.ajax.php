<?php
require_once "../controllers/pagos.controller.php";
require_once "../models/pagos.model.php";

class AjaxPayments {
    public $idPago;
    
    public function ajaxEditPayment() {
        $item = "id_payment";
        $valor = $this->idPago;
        $respuesta = ControllerPayment::ctrlShowPayments($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idPago"])) {
    $editar = new AjaxPayments();
    $editar->idPago = $_POST["idPago"];
    $editar->ajaxEditPayment();
}
