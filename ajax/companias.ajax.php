<?php
require_once "../controllers/companias.controller.php";
require_once "../models/companias.model.php";

class AjaxCompanies {
    public $idCompania;
    
    public function ajaxEditCompany() {
        $item = "id_business";
        $valor = $this->idCompania;
        $respuesta = ControllerCompany::ctrlShowCompanies($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idCompania"])) {
    $editar = new AjaxCompanies();
    $editar->idCompania = $_POST["idCompania"];
    $editar->ajaxEditCompany();
}
