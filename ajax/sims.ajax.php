<?php
require_once "../controllers/sims.controller.php";
require_once "../models/sims.model.php";

class AjaxSim {
    public $idSim;

    public function ajaxEditSim() {
        $item = "id_sim";
        $valor = $this->idSim;
        $respuesta = ControllerSim::ctrlShowSims($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idSim"])) {
    $editar = new AjaxSim();
    $editar->idSim = $_POST["idSim"];
    $editar->ajaxEditSim();
}