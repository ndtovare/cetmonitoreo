<?php
require_once "../controllers/productos.controller.php";
require_once "../models/productos.model.php";

class AjaxProducts {
    public $idProducto;
    
    public function ajaxEditProduct() {
        $item = "id_product";
        $valor = $this->idProducto;
        $respuesta = ControllerProduct::ctrlShowProducts($item, $valor, 0);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idProducto"])) {
    $editar = new AjaxProducts();
    $editar->idProducto = $_POST["idProducto"];
    $editar->ajaxEditProduct();
}

if(isset($_POST["idEquipoInstalado"])) {
    $editar = new AjaxProducts();
    $editar->idProducto = $_POST["idEquipoInstalado"];
    $editar->ajaxEditProduct();
}
