<?php
require_once "../controllers/transportes-publicos.controller.php";
require_once "../models/transportes-publicos.model.php";

class AjaxPublicTransports {
    public $idTransportePublico;
    
    public function ajaxEditPublicTransport() {
        $item = "id_vehicle";
        $valor = $this->idTransportePublico;
        $respuesta = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idTransportePublico"])) {
    $editar = new AjaxPublicTransports();
    $editar->idTransportePublico = $_POST["idTransportePublico"];
    $editar->ajaxEditPublicTransport();
}
