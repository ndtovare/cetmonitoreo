<?php
require_once "../controllers/vehiculos-privados.controller.php";
require_once "../models/vehiculos-privados.model.php";

class AjaxPrivateVehicles {
    public $idVehiculoPrivado;
    
    public function ajaxEditPrivateVehicle() {
        $item = "id_vehicle";
        $valor = $this->idVehiculoPrivado;
        $respuesta = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idVehiculoPrivado"])) {
    $editar = new AjaxPrivateVehicles();
    $editar->idVehiculoPrivado = $_POST["idVehiculoPrivado"];
    $editar->ajaxEditPrivateVehicle();
}
