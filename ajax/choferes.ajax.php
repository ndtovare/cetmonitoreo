<?php
require_once "../controllers/choferes.controller.php";
require_once "../models/choferes.model.php";

class AjaxDrivers {
    public $idPropietario;
    
    public function ajaxEditDriver() {
        $item = "id_driver";
        $valor = $this->idChofer;
        $respuesta = ControllerDriver::ctrlShowDrivers($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idChofer"])) {
    $editar = new AjaxDrivers();
    $editar->idChofer = $_POST["idChofer"];
    $editar->ajaxEditDriver();
}
