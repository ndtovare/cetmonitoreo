<?php
require_once "../controllers/flotillas.controller.php";
require_once "../models/flotillas.model.php";

class AjaxFleet {
    public $idFlotilla;
    
    public function ajaxEditFleet() {
        $item = "id_fleet";
        $valor = $this->idFlotilla;
        $respuesta = ControllerFleet::ctrlShowFleets($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idFlotilla"])) {
    $editar = new AjaxFleet();
    $editar->idFlotilla = $_POST["idFlotilla"];
    $editar->ajaxEditFleet();
}
