<?php
require_once "../controllers/codigos.controller.php";
require_once "../models/codigos.model.php";

class AjaxCodes {
    public $idCodigo;

    public function ajaxEditCode() {
        $item = "id_code";
        $valor = $this->idCodigo;
        $respuesta = ControllerCode::ctrlShowCodes($item, $valor);
        echo json_encode($respuesta);
    }

    public function ajaxValidateCodes() {
        $respuesta = ControllerCode::ctrlValidateCodes();
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idCodigo"])) {
    $editar = new AjaxCodes();
    $editar->idCodigo = $_POST["idCodigo"];
    $editar->ajaxEditCode();
}

if(isset($_POST["validaCodigo"])) {
    $validar = new AjaxCodes();
    $validar->ajaxValidateCodes();
}