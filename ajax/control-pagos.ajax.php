<?php
require_once "../controllers/control-pagos.controller.php";
require_once "../models/control-pagos.model.php";

class AjaxPaymentControl {
    public $idControlPago;
    
    public function ajaxEditPaymentControl() {
        $item = "id_payment_control";
        $valor = $this->idControlPago;
        $respuesta = ControllerPaymentControl::ctrlShowPaymentControl($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idControlPago"])) {
    $editar = new AjaxPaymentControl();
    $editar->idControlPago = $_POST["idControlPago"];
    $editar->ajaxEditPaymentControl();
}
