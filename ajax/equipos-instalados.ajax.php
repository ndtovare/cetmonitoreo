<?php
require_once "../controllers/equipos-instalados.controller.php";
require_once "../models/equipos-instalados.model.php";

class AjaxInstalledEquipment {
    public $idEquipoInstalado;

    public function ajaxEditInstalledEquipment() {
        $item = "id_installed_equipment";
        $valor = $this->idEquipoInstalado;
        $respuesta = ControllerInstalledEquipment::ctrlShowInstalledEquipment($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idEquipoInstalado"])) {
    $editar = new AjaxInstalledEquipment();
    $editar->idEquipoInstalado = $_POST["idEquipoInstalado"];
    $editar->ajaxEditInstalledEquipment();
}