<?php
require_once "../controllers/usuarios.controller.php";
require_once "../models/usuarios.model.php";

class AjaxUsers {
    public $idUsuario;
    
    public function ajaxEditUser() {
        $item = "id_user";
        $valor = $this->idUsuario;
        $respuesta = ControllerUser::ctrlShowUsers($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idUsuario"])) {
    $editar = new AjaxUsers();
    $editar->idUsuario = $_POST["idUsuario"];
    $editar->ajaxEditUser();
}
