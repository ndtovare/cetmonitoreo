<?php
require_once "../controllers/propietarios.controller.php";
require_once "../models/propietarios.model.php";

class AjaxOwners {
    public $idPropietario;
    
    public function ajaxEditOwner() {
        $item = "id_client";
        $valor = $this->idPropietario;
        echo '<script>console.log('.json_encode($valor).')</script>';
        echo '<script>console.log('.json_encode($item).')</script>';
        $respuesta = ControllerOwner::ctrlShowOwners($item, $valor);
        echo json_encode($respuesta);
    }
}

if(isset($_POST["idPropietario"])) {
    $editar = new AjaxOwners();
    $editar->idPropietario = $_POST["idPropietario"];
    echo '<script>console.log('.json_encode($editar).')</script>';
    $editar->ajaxEditOwner();
} else {
    echo '<script>console.log('.json_encode('error').')</script>';
}
