<header class="main-header">
    <a href="inicio" class="logo">
        <span class="logo-mini">
            <img src="views/img/plantilla/cet-icono.png" class="img-responsive">
        </span>
        <span class="logo-lg">
            <img src="views/img/plantilla/cet-logo.png" class="img-responsive">
        </span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle Navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        if($_SESSION["foto"] != "") {
                            echo '<img src="'.$_SESSION["foto"].'" class="user-image">';
                        } else {
                            echo '<img src="views/img/usuarios/default/anonymous.png" class="user-image">';
                        }
                        ?>
                        <span class="hidden-xs"><?php echo $_SESSION["nombre"]; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-body">
                            <div class="pull-right">
                                <a href="salir" class="btn btn-default btn-flat">Salir</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>