<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar sims
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar sims</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarSim">Agregar sims</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Estatus</th>
              <th style="text-align: center">Número de serie</th>
              <th style="text-align: center">Número</th>
              <th style="text-align: center">Esquema</th>
              <th style="text-align: center">Saldo</th>
              <th style="text-align: center">Fecha</th>
              <th style="text-align: center">Fecha de corte</th>
              <th style="text-align: center">Código</th>
              <th style="text-align: center">Lote</th>
              <th style="text-align: center">Usuario</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $sims = ControllerSim::ctrlShowSims($item, $valor);
            
            foreach ($sims as $key => $value) {
              $fechaSimEditada = $value["date_sim"];
              $fechaCorteSimEditada = $value["cutting_day_sim"];
              $timeStampFecha = strtotime($fechaSimEditada);
              $timeStampCorte = strtotime($fechaCorteSimEditada);
              $fechaSimOriginal = date("d/m/Y", $timeStampFecha);
              $fechaCorteOriginal = date("d/m/Y", $timeStampCorte);
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["status_sim"].'</td>
                  <td style="text-align: center">'.$value["serial_number_sim"].'</td>
                  <td style="text-align: center">'.$value["number_sim"].'</td>
                  <td style="text-align: center">'.$value["scheme_sim"].'</td>
                  <td style="text-align: center">'.$value["balance_sim"].'</td>
                  <td style="text-align: center">'.$fechaSimOriginal.'</td>
                  <td style="text-align: center">'.$fechaCorteOriginal.'</td>
                  <td style="text-align: center">'.$value["name_code"].'</td>
                  <td style="text-align: center">'.$value["bill_lot"].'</td>
                  <td style="text-align: center">'.$value["nickname_user"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarSim" idSim="'.$value["id_sim"].'" data-toggle="modal" data-target="#modalEditarSim"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarSim" idSim="'.$value["id_sim"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarSim" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar sim</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-wrench"></i></span>
                <select class="form-control input-lg" name="nuevoEstatus">
                  <option value="">Seleccionar estatus</option>
                  <option value="Disponible">Disponible</option>
                  <option value="Instalado">Instalado</option>
                  <option value="Recuperación">Recuperación</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNumeroSerie" placeholder="Ingresa el número de serie">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-wifi"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNumeroSim" placeholder="Ingresa el número de sim">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <select class="form-control input-lg" name="nuevoEsquema">
                  <option value="">Seleccionar esquema</option>
                  <option value="Negocio 70">Negocio 70</option>
                  <option value="Negocio 139">Negocio 139</option>
                  <option value="Negocio 500">Negocio 500</option>
                  <option value="Amigo 250">Amigo 250</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoSaldo" placeholder="Ingresa el saldo">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="nuevaFecha" placeholder="Ingresa la fecha">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="nuevaFechaCorte" placeholder="Ingresa la fecha de corte">
                </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="nuevoCodigo">
                  <option value="">Seleccionar código</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $codigos = ControllerCode::ctrlShowCodes($item, $valor);

                  foreach ($codigos as $key => $value) {
                    echo '<option value="'.$value["id_code"].'">'.$value["name_code"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                <select class="form-control input-lg" name="nuevoLote">
                  <option value="">Seleccionar lote</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $lotes = ControllerLot::ctrlShowLots($item, $valor);

                  foreach ($lotes as $key => $value) {
                    echo '<option value="'.$value["id_lot"].'">'.$value["bill_lot"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar sim</button>
        </div>
      </form>
      <?php
      $crearSim = new ControllerSim();
      $crearSim->ctrlInsertSim();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarSim" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar sim</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-wrench"></i></span>
                <select class="form-control input-lg" name="editarEstatus" id="editarEstatus">
                  <option></option>
                  <option value="Disponible">Disponible</option>
                  <option value="Instalado">Instalado</option>
                  <option value="Recuperación">Recuperación</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarNumeroSerie" id="editarNumeroSerie">
                <input type="hidden" name="idSim" id="idSim">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-wifi"></i></span>
                <input type="text" class="form-control input-lg" name="editarNumeroSim" id="editarNumeroSim">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <select class="form-control input-lg" name="editarEsquema" id="editarEsquema">
                  <option></option>
                  <option value="Negocio 70">Negocio 70</option>
                  <option value="Negocio 139">Negocio 139</option>
                  <option value="Negocio 500">Negocio 500</option>
                  <option value="Amigo 250">Amigo 250</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="editarSaldo" id="editarSaldo">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFecha" id="editarFecha">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFechaCorte" id="editarFechaCorte">
                </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="editarCodigo" id="editarCodigo">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $codigos = ControllerCode::ctrlShowCodes($item, $valor);

                  foreach ($codigos as $key => $value) {
                    echo '<option value="'.$value["id_code"].'">'.$value["name_code"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                <select class="form-control input-lg" name="editarLote" id="editarLote">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $lotes = ControllerLot::ctrlShowLots($item, $valor);

                  foreach ($lotes as $key => $value) {
                    echo '<option value="'.$value["id_lot"].'">'.$value["bill_lot"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar sim</button>
        </div>
      </form>
      <?php
      $editarSim = new ControllerSim();
      $editarSim->ctrlEditSim();
      ?>
    </div>
  </div>
</div>
<?php
$borrarSim = new ControllerSim();
$borrarSim->ctrlDeleteSim();
?>