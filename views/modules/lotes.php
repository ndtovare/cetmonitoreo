<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar lotes
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar lotes</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarLote">Agregar lote</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Factura</th>
              <th style="text-align: center">Fecha de admisión</th>
              <th style="text-align: center">Fecha de garantía</th>
              <th style="text-align: center">Usuario</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $lotes = ControllerLot::ctrlShowLots($item, $valor);
            
            foreach ($lotes as $key => $value) {
              $fechaAdmisionEditada = $value["date_admission_lot"];
              $fechaGarantiaEditada = $value["warranty_date_lot"];
              $timeStampAdmision = strtotime($fechaAdmisionEditada);
              $timeStampGarantia = strtotime($fechaGarantiaEditada);
              $fechaAdmisionOriginal = date("d/m/Y", $timeStampAdmision);
              $fechaGarantiaOriginal = date("d/m/Y", $timeStampGarantia);
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["bill_lot"].'</td>
                  <td style="text-align: center">'.$fechaAdmisionOriginal.'</td>
                  <td style="text-align: center">'.$fechaGarantiaOriginal.'</td>
                  <td style="text-align: center">'.$value["nickname_user"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarLote" idLote="'.$value["id_lot"].'" data-toggle="modal" data-target="#modalEditarLote"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarLote" idLote="'.$value["id_lot"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarLote" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar lote</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevaFactura" placeholder="Ingresa la factura">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="nuevaFechaAdmision" placeholder="Ingresa la fecha de admisión">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="nuevaFechaGarantia" placeholder="Ingresa la fecha de garantía">
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar lote</button>
        </div>
      </form>
      <?php
      $crearLote = new ControllerLot();
      $crearLote->ctrlInsertLot();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarLote" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar lote</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarFactura" id="editarFactura">
                <input type="hidden" name="idLote" id="idLote">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFechaAdmision" id="editarFechaAdmision">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFechaGarantia" id="editarFechaGarantia">
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar lote</button>
        </div>
      </form>
      <?php
      $editarLote = new ControllerLot();
      $editarLote->ctrlEditLot();
      ?>
    </div>
  </div>
</div>
<?php
$borrarLote = new ControllerLot();
$borrarLote->ctrlDeleteLot();
?>