<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar flotillas
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar flotillas</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarFlotilla">Agregar flotilla</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Nombre</th>
              <th style="text-align: center">Dirección</th>
              <th style="text-align: center">Nombres de contacto</th>
              <th style="text-align: center">Apellidos de contacto</th>
              <th style="text-align: center">Teléfono de contacto</th>
              <th style="text-align: center">Identificador de vehículo</th>
              <th style="text-align: center">Tipo de vehículo</th>
              <th style="text-align: center">Compañía</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $flotillas = ControllerFleet::ctrlShowFleets($item, $valor);
            
            foreach ($flotillas as $key => $value) {
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["name_fleet"].'</td>
                  <td style="text-align: center">'.$value["address_fleet"].'</td>
                  <td style="text-align: center">'.$value["name_contact_fleet"].'</td>
                  <td style="text-align: center">'.$value["last_name_contact_fleet"].'</td>
                  <td style="text-align: center">'.$value["phone_contact_fleet"].'</td>
                  <td style="text-align: center">'.$value["identifier_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["type_vehicle"].'</td>
                  <td style="text-align: center">'.$value["name_business"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarFlotilla" idFlotilla="'.$value["id_fleet"].'" data-toggle="modal" data-target="#modalEditarFlotilla"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarFlotilla" idFlotilla="'.$value["id_fleet"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarFlotilla" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar flotilla</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNombreFlotilla" placeholder="Ingresa el nombre">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" class="form-control input-lg" name="nuevaDireccionFlotilla" placeholder="Ingresa la dirección">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNombreContactoFlotilla" placeholder="Ingresa los nombres del contacto">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoApellidoContactoFlotilla" placeholder="Ingresa los apellidos del contacto">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="nuevoTelefonoFlotilla" placeholder="Ingresa el teléfono del contacto">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="seleccionaVehiculoFlotilla" id="seleccionaVehiculoFlotilla">
                  <option value="">Selecciona un tipo de vehículo</option>
                  <option value="TP">Transporte público</option>
                  <option value="VP">Vehículo privado</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="nuevoTransportePublicoFlotilla">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="nuevoTransportePublico">
                  <option value="">Seleccionar transporte público</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="nuevoVehiculoPrivadoFlotilla">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="nuevoVehiculoPrivado">
                  <option value="">Seleccionar vehículo privado</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                <select class="form-control input-lg" name="nuevaCompania">
                  <option value="">Seleccionar compañía</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $compania = ControllerCompany::ctrlShowCompanies($item, $valor);

                  foreach ($compania as $key => $value) {
                    echo '<option value="'.$value["id_business"].'">'.$value["name_business"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar flotilla</button>
        </div>
      </form>
      <?php
      $crearFlotilla = new ControllerFleet();
      $crearFlotilla->ctrlInsertFleet();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarFlotilla" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar flotilla</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreFlotilla" id="editarNombreFlotilla">
                <input type="hidden" name="idFlotilla" id="idFlotilla">
                <input type="hidden" name="editarSeleccionaVehiculoF" id="editarSeleccionaVehiculoF">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" class="form-control input-lg" name="editarDireccionFlotilla" id="editarDireccionFlotilla">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreContactoFlotilla" id="editarNombreContactoFlotilla">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarApellidoContactoFlotilla" id="editarApellidoContactoFlotilla">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="editarTelefonoFlotilla" id="editarTelefonoFlotilla">
              </div>
            </div>
            <div class="form-group" id="editarTransportePublicoFlotilla">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="editarTransportePublico" id="editarTransportePublico">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="editarVehiculoPrivadoFlotilla">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="editarVehiculoPrivado" id="editarVehiculoPrivado">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                <select class="form-control input-lg" name="editarCompania" id="editarCompania">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $compania = ControllerCompany::ctrlShowCompanies($item, $valor);

                  foreach ($compania as $key => $value) {
                    echo '<option value="'.$value["id_business"].'">'.$value["name_business"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar flotilla</button>
        </div>
      </form>
      <?php
      $editarFlotilla = new ControllerFleet();
      $editarFlotilla->ctrlEditFleet();
      ?>
    </div>
  </div>
</div>
<?php
$borrarFlotilla = new ControllerFleet();
$borrarFlotilla->ctrlDeleteFleet();
?>