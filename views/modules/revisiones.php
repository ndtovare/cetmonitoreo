<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar revisiones
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar revisiones</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarRevision">Agregar revisión</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Identificador</th>
              <th style="text-align: center">Equipo Instalado</th>
              <th style="text-align: center">Comentarios</th>
              <th style="text-align: center">Fecha de revisión</th>
              <th style="text-align: center">Precio</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $revisiones = ControllerRevision::ctrlShowRevisions($item, $valor);
            
            foreach ($revisiones as $key => $value) {
              $fechaRevisionEditada = $value["date_revision"];
              $timeStampRevision = strtotime($fechaRevisionEditada);
              $fechaRevisionOriginal = date("d/m/Y", $timeStampRevision);
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["identifier_revision"].'</td>
                  <td style="text-align: center">'.$value["name_code"].'</td>
                  <td style="text-align: center">'.$value["comments_revision"].'</td>
                  <td style="text-align: center">'.$fechaRevisionOriginal.'</td>
                  <td style="text-align: center">'.$value["price_revision"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarRevision" idRevision="'.$value["id_revision"].'" data-toggle="modal" data-target="#modalEditarRevision"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarRevision" idRevision="'.$value["id_revision"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarRevision" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar revision</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoIdentificador" placeholder="Ingresa el identificador">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoComentario" placeholder="Ingresa el comentario">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="nuevaFechaRevision" placeholder="Ingresa la fecha de revisión">
                </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoPrecio" placeholder="Ingresa el precio">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="nuevoEquipoInstalado">
                  <option value="">Seleccionar equipo instalado</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $equiposInstalados = ControllerInstalledEquipment::ctrlShowInstalledEquipment($item, $valor);

                  foreach ($equiposInstalados as $key => $value) {
                    echo '<option value="'.$value["id_installed_equipment"].'">'.$value["name_code"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar revision</button>
        </div>
      </form>
      <?php
      $crearRevision = new ControllerRevision();
      $crearRevision->ctrlInsertRevision();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarRevision" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar revisión</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarIdentificador" id="editarIdentificador">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarComentario" id="editarComentario">
                <input type="hidden" name="idRevision" id="idRevision">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFechaRevision" id="editarFechaRevision">
                </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarPrecio" id="editarPrecio">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="editarEquipoInstalado" id="editarEquipoInstalado">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $equiposInstalados = ControllerInstalledEquipment::ctrlShowInstalledEquipment($item, $valor);

                  foreach ($equiposInstalados as $key => $value) {
                    echo '<option value="'.$value["id_installed_equipment"].'">'.$value["name_code"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar revisión</button>
        </div>
      </form>
      <?php
      $editarRevision = new ControllerRevision();
      $editarRevision->ctrlEditRevision();
      ?>
    </div>
  </div>
</div>
<?php
$borrarRevision = new ControllerRevision();
$borrarRevision->ctrlDeleteRevision();
?>