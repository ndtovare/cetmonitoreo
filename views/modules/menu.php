<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php
            if($_SESSION["perfil"] == "Administrador" ||
               $_SESSION["perfil"] == "Supervisor") {
                echo '
                <li>
                    <a href="inicio">
                        <i class="fa fa-home"></i>
                        <span>Inicio</span>
                    </a>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li>
                    <a href="usuarios">
                        <i class="fa fa-users"></i>
                        <span>Usuarios</span>
                    </a>
                </li>
                ';
            }
            
            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cubes"></i>
                        <span>Productos</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="validarProductos">
                            <a href="productos">
                                <i class="fa fa-cube"></i>
                                <span>Administrar productos</span>
                            </a>
                        </li>
                        <li>
                            <a href="lotes">
                                <i class="fa fa-cubes"></i>
                                <span>Administrar lotes</span>
                            </a>
                        </li>
                        <li>
                            <a href="codigos">
                                <i class="fa fa-barcode"></i>
                                <span>Administrar códigos</span>
                            </a>
                        </li>
                    </ul>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Clientes</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="clientes">
                                <i class="fa fa-user"></i>
                                <span>Administrar clientes</span>
                            </a>
                        </li>
                        <li>
                            <a href="propietarios">
                                <i class="fa fa-user"></i>
                                <span>Administrar propietarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="choferes">
                                <i class="fa fa-user"></i>
                                <span>Administrar chóferes</span>
                            </a>
                        </li>
                        <li>
                            <a href="usuarios-genericos">
                                <i class="fa fa-user"></i>
                                <span>Administrar usuarios genéricos</span>
                            </a>
                        </li>
                    </ul>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-automobile"></i>
                        <span>Vehículos</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="vehiculos">
                                <i class="fa fa-automobile"></i>
                                <span>Agregar vehículos</span>
                            </a>
                        </li>
                        <li>
                            <a href="transportes-publicos">
                                <i class="fa fa-bus"></i>
                                <span>Administrar transportes públicos</span>
                            </a>
                        </li>
                        <li>
                            <a href="vehiculos-privados">
                                <i class="fa fa-automobile"></i>
                                <span>Administrar vehículos privados</span>
                            </a>
                        </li>
                        <li>
                            <a href="flotillas">
                                <i class="fa fa-truck"></i>
                                <span>Administrar flotillas</span>
                            </a>
                        </li>
                    </ul>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li>
                    <a href="companias">
                        <i class="fa fa-industry"></i>
                        <span>Compañías</span>
                    </a>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li>
                    <a href="sims">
                        <i class="fa fa-wifi"></i>
                        <span>Sims</span>
                    </a>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-credit-card"></i>
                        <span>Pagos</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="pagos">
                                <i class="fa fa-money"></i>
                                <span>Administrar pagos</span>
                            </a>
                        </li>
                        <li>
                            <a href="control-pagos">
                                <i class="fa fa-cc-visa"></i>
                                <span>Administrar control de pagos</span>
                            </a>
                        </li>
                    </ul>
                </li>
                ';
            }

            if($_SESSION["perfil"] == "Administrador") {
                echo '
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span>Equipos instalados</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="equipos-instalados">
                                <i class="fa fa-cog"></i>
                                <span>Administrar equipos instalados</span>
                            </a>
                        </li>
                        <li>
                            <a href="revisiones">
                                <i class="fa fa-eye"></i>
                                <span>Administrar revisiones</span>
                            </a>
                        </li>
                    </ul>
                </li>
                ';
            }
            ?>
        </ul>
    </section>
</aside>