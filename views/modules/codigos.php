<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar códigos
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar códigos</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCodigo">Agregar código</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Foto</th>
              <th style="text-align: center">Nombre</th>
              <th style="text-align: center">Descripción</th>
              <th style="text-align: center">Precio</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $codigos = ControllerCode::ctrlShowCodes($item, $valor);
            
            foreach ($codigos as $key => $value) {
              echo '
                <tr>
                  <td>'.($key + 1).'</td>';
              if($value["image_code"] != "") {
                echo '<td style="text-align: center"><img src="'.$value["image_code"].'" class="img-thumbnail" width="60px;"></td>';
              } else {
                echo '<td style="text-align: center"><img src="views/img/productos/default/anonymous.png" class="img-thumbnail" width="60px;"></td>';
              }
              echo '
                  <td style="text-align: center">'.$value["name_code"].'</td>
                  <td style="text-align: center">'.$value["description_code"].'</td>
                  <td style="text-align: center">'.$value["price_code"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarCodigo" idCodigo="'.$value["id_code"].'" data-toggle="modal" data-target="#modalEditarCodigo"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarCodigo" idCodigo="'.$value["id_code"].'" fotoCodigo="'.$value["image_code"].'" codigo="'.$value["name_code"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarCodigo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar código</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoCodigo" placeholder="Ingresa el código">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                <input type="text" class="form-control input-lg" name="nuevaDescripcion" placeholder="Ingresa la descripción">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoPrecio" placeholder="Ingresa el precio">
                <span class="input-group-addon">.00</span>
              </div>
            </div>
            <div class="form-group">
              <div class="panel">SUBIR FOTO</div>
              <input type="file" class="nuevaFoto" name="nuevaFoto">
              <p class="help-block">Peso máximo de la foto 2MB</p>
              <img src="views/img/productos/default/anonymous.png" class="img-thumbnail previsualizar" width="100px;">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar código</button>
        </div>
      </form>
      <?php
      $crearCodigo = new ControllerCode();
      $crearCodigo->ctrlInsertCode();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarCodigo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar código</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarCodigo" id="editarCodigo">
                <input type="hidden" id="idCodigo" name="idCodigo">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                <input type="text" class="form-control input-lg" name="editarDescripcion" id="editarDescripcion">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" class="form-control input-lg" name="editarPrecio" id="editarPrecio">
                <span class="input-group-addon">.00</span>
              </div>
            </div>
            <div class="form-group">
              <div class="panel">SUBIR FOTO</div>
              <input type="file" class="nuevaFoto" name="editarFoto">
              <p class="help-block">Peso máximo de la foto 2MB</p>
              <img src="views/img/productos/default/anonymous.png" class="img-thumbnail previsualizar" width="100px;">
              <input type="hidden" name="fotoActual" id="fotoActual">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar código</button>
        </div>
      </form>
      <?php
      $editarCodigo = new ControllerCode();
      $editarCodigo->ctrlEditCode();
      ?>
    </div>
  </div>
</div>
<?php
$borrarCodigo = new ControllerCode();
$borrarCodigo->ctrlDeleteCode();
?>