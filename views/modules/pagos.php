<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar pagos
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar pagos</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarPago">Agregar pago</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Identificador</th>
              <th style="text-align: center">IMEI</th>
              <th style="text-align: center">Método</th>
              <th style="text-align: center">Número</th>
              <th style="text-align: center">Fecha</th>
              <th style="text-align: center">Monto</th>
              <th style="text-align: center">Compañía</th>
              <th style="text-align: center">Propietario</th>
              <th style="text-align: center">Identificador de vehículo</th>
              <th style="text-align: center">Tipo de vehículo</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $pagos = ControllerPayment::ctrlShowPayments($item, $valor);
            
            foreach ($pagos as $key => $value) {
              $fechaCompraEditada = $value["date_payment"];
              $timeStampCompra = strtotime($fechaCompraEditada);
              $fechaCompraOriginal = date("d/m/Y", $timeStampCompra);
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["identifier_payment"].'</td>
                  <td style="text-align: center">'.$value["imei_payment"].'</td>
                  <td style="text-align: center">'.$value["method_payment"].'</td>
                  <td style="text-align: center">'.$value["number_payment"].'</td>
                  <td style="text-align: center">'.$fechaCompraOriginal.'</td>
                  <td style="text-align: center">'.$value["amount_payment"].'</td>
                  <td style="text-align: center">'.$value["name_business"].'</td>
                  <td style="text-align: center">'.$value["name_owner"].'</td>
                  <td style="text-align: center">'.$value["identifier_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["type_vehicle"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarPago" idPago="'.$value["id_payment"].'" data-toggle="modal" data-target="#modalEditarPago"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarPago" idPago="'.$value["id_payment"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarPago" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar pago</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoIdentificadorPago" placeholder="Ingresa el identificador">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                <select class="form-control input-lg" name="nuevoMes">
                  <option value="">Seleccionar mes</option>
                  <option value="Enero">Enero</option>
                  <option value="Febrero">Febrero</option>
                  <option value="Marzo">Marzo</option>
                  <option value="Abril">Abril</option>
                  <option value="Mayo">Mayo</option>
                  <option value="Junio">Junio</option>
                  <option value="Julio">Julio</option>
                  <option value="Agosto">Agosto</option>
                  <option value="Septiembre">Septiembre</option>
                  <option value="Octubre">Octubre</option>
                  <option value="Noviembre">Noviembre</option>
                  <option value="Diciembre">Diciembre</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                <select class="form-control input-lg" name="nuevoMetodoPago">
                  <option value="">Seleccionar método de pago</option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Transferencia">Transferencia</option>
                  <option value="Depósito">Depósito</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNumeroPago" placeholder="Ingresa el número de pago">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="nuevaFechaPago" placeholder="Ingresa la fecha de pago">
                </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoMontoPago" placeholder="Ingresa el monto">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar pago</button>
        </div>
      </form>
      <?php
      $crearPago = new ControllerPayment();
      $crearPago->ctrlInsertPayment();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarPago" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar pago</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarIdentificadorPago" id="editarIdentificadorPago">
                <input type="hidden" name="idPago" id="idPago">
                <input type="hidden" name="editarSeleccionaVehiculoP" id="editarSeleccionaVehiculoP">
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                  <select class="form-control input-lg" name="editarMes" id="editarMes">
                    <option value="">Seleccionar mes</option>
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                <select class="form-control input-lg" name="editarMetodoPago" id="editarMetodoPago">
                  <option></option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Transferencia">Transferencia</option>
                  <option value="Depósito">Depósito</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarNumeroPago" id="editarNumeroPago">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFechaPago" id="editarFechaPago">
                </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                <input type="text" class="form-control input-lg" name="editarMontoPago" id="editarMontoPago">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                <select class="form-control input-lg" name="editarCompania" id="editarCompania">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $companias = ControllerCompany::ctrlShowCompanies($item, $valor);

                  foreach ($companias as $key => $value) {
                    echo '<option value="'.$value["id_business"].'">'.$value["name_business"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <select class="form-control input-lg" name="editarPropietario" id="editarPropietario">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $propietarios = ControllerOwner::ctrlShowOwners($item, $valor);

                  foreach ($propietarios as $key => $value) {
                    echo '<option value="'.$value["id_owner"].'">'.$value["name_owner"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="editarTransportePublicoPago">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="editarTransportePublico" id="editarTransportePublico">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="editarVehiculoPrivadoPago">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="editarVehiculoPrivado" id="editarVehiculoPrivado">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar pago</button>
        </div>
      </form>
      <?php
      $editarPago = new ControllerPayment();
      $editarPago->ctrlEditPayment();
      ?>
    </div>
  </div>
</div>
<?php
$borrarPago = new ControllerPayment();
$borrarPago->ctrlDeletePayment();
?>