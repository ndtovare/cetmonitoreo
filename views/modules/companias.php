<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar compañías
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar compañías</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCompania">Agregar compañía</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Nombre</th>
              <th style="text-align: center">Acrónimo</th>
              <th style="text-align: center">Correo electrónico</th>
              <th style="text-align: center">Teléfono</th>
              <th style="text-align: center">Nombres del representante</th>
              <th style="text-align: center">Apellidos del representante</th>
              <th style="text-align: center">Teléfono del representante</th>
              <th style="text-align: center">Usuario</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $companias = ControllerCompany::ctrlShowCompanies($item, $valor);
            
            foreach ($companias as $key => $value) {
              echo '
                <tr>
                  <td style="text-align: center">'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["name_business"].'</td>
                  <td style="text-align: center">'.$value["acronym_business"].'</td>
                  <td style="text-align: center">'.$value["email_business"].'</td>
                  <td style="text-align: center">'.$value["phone_business"].'</td>
                  <td style="text-align: center">'.$value["representative_name_business"].'</td>
                  <td style="text-align: center">'.$value["representative_last_name_business"].'</td>
                  <td style="text-align: center">'.$value["representative_phone_business"].'</td>
                  <td style="text-align: center">'.$value["nickname_user"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarCompania" idCompania="'.$value["id_business"].'" data-toggle="modal" data-target="#modalEditarCompania"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarCompania" idCompania="'.$value["id_business"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarCompania" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar compañía</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNombreCompania" placeholder="Ingresa el nombre">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoAcronimoCompania" placeholder="Ingresa el acrónimo">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="nuevoTelefonoCompania" placeholder="Ingresa el teléfono">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" class="form-control input-lg" name="nuevaDireccionCompania" placeholder="Ingresa la dirección">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control input-lg" name="nuevoCorreoCompania" placeholder="Ingresa el correo electrónico" rquired>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNombreRepresentanteCompania" placeholder="Ingresa los nombres del representante">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoApellidoRepresentanteCompania" placeholder="Ingresa los apellidos del representante">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="nuevoTelefonoRepresentanteCompania" placeholder="Ingresa el teléfono del representante">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoCorreoRepresentanteCompania" placeholder="Ingresa el correo electrónico del representante">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNombreReferenciaCompania" placeholder="Ingresa los nombres de referencia">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoApellidoReferenciaCompania" placeholder="Ingresa los apellidos de referencia">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="nuevoTelefonoReferenciaCompania" placeholder="Ingresa el teléfono de referencia">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar compañía</button>
        </div>
      </form>
      <?php
      $crearCompania = new ControllerCompany();
      $crearCompania->ctrlInsertCompany();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarCompania" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar compañía</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreCompania" id="editarNombreCompania">
                <input type="hidden" name="idCompania" id="idCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                <input type="text" class="form-control input-lg" name="editarAcronimoCompania" id="editarAcronimoCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="editarTelefonoCompania" id="editarTelefonoCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" class="form-control input-lg" name="editarDireccionCompania" id="editarDireccionCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control input-lg" name="editarCorreoCompania" id="editarCorreoCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreRepresentanteCompania" id="editarNombreRepresentanteCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarApellidoRepresentanteCompania" id="editarApellidoRepresentanteCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="editarTelefonoRepresentanteCompania" id="editarTelefonoRepresentanteCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control input-lg" name="editarCorreoRepresentanteCompania" id="editarCorreoRepresentanteCompania" required>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreReferenciaCompania" id="editarNombreReferenciaCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarApellidoReferenciaCompania" id="editarApellidoReferenciaCompania">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="editarTelefonoReferenciaCompania" id="editarTelefonoReferenciaCompania">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar compañía</button>
        </div>
      </form>
      <?php
      $editarCompania = new ControllerCompany();
      $editarCompania->ctrlEditCompany();
      ?>
    </div>
  </div>
</div>
<?php
$borrarCompania = new ControllerCompany();
$borrarCompania->ctrlDeleteCompany();
?>