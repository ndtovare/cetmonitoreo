<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar vehículos privados
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar vehículos privados</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">IMEI</th>
              <th style="text-align: center">Identificador</th>
              <th style="text-align: center">Serie vehicular</th>
              <th style="text-align: center">Marca</th>
              <th style="text-align: center">Modelo</th>
              <th style="text-align: center">Año</th>
              <th style="text-align: center">Número de placa</th>
              <th style="text-align: center">Tipo</th>
              <th style="text-align: center">Propietario</th>
              <th style="text-align: center">Chófer</th>
              <th style="text-align: center">Usuario</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);
            
            foreach ($vehiculosPrivados as $key => $value) {
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["imei_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["identifier_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["vehicular_series_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["brand_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["model_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["year_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["plate_number_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["class_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["owner_vehicle"].'</td>
                  <td style="text-align: center">'.$value["driver_vehicle"].'</td>
                  <td style="text-align: center">'.$value["nickname_user"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarVehiculoPrivado" idVehiculoPrivado="'.$value["id_vehicle"].'" data-toggle="modal" data-target="#modalEditarTransportePublico"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarVehiculoPrivado" idVehiculoPrivado="'.$value["id_vehicle"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalEditarTransportePublico" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar vehiculo privado</h4>
        </div>
        <div class="modal-body">
        <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarImeiTP" id="editarImeiTP">
                <input type="hidden" name="idVehiculoPrivado" id="idVehiculoPrivado">
                <input type="hidden" name="tipoVehiculo" id="tipoVehiculo">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarIdentificadorTP" id="editarIdentificadorTP">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                <input type="text" class="form-control input-lg" name="editarMarcaTP" id="editarMarcaTP">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-gear"></i></span>
                <input type="text" class="form-control input-lg" name="editarModeloTP" id="editarModeloTP">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" class="form-control input-lg" name="editarAnioTP" id="editarAnioTP">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarSerieTP" id="editarSerieTP">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarPlacaTP" id="editarPlacaTP">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="editarTipoTP" id="editarTipoTP">
                  <option></option>
                  <option value="Autobús">Autobús</option>
                  <option value="Sedán">Sedán</option>
                  <option value="Vagoneta">Vagoneta</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                <select class="form-control input-lg" name="editarPropietario" id="editarPropietario">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $propietarios = ControllerOwner::ctrlShowOwners($item, $valor);

                  foreach ($propietarios as $key => $value) {
                    echo '<option value="'.$value["id_owner"].'">'.$value["name_owner"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                <select class="form-control input-lg" name="editarChofer" id="editarChofer">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $choferes = ControllerDriver::ctrlShowDrivers($item, $valor);

                  foreach ($choferes as $key => $value) {
                    echo '<option value="'.$value["id_driver"].'">'.$value["name_driver"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar vehículo privado</button>
        </div>
      </form>
      <?php
      $editarVehiculoPrivado = new ControllerPrivateVehicle();
      $editarVehiculoPrivado->ctrlEditPrivateVehicle();
      ?>
    </div>
  </div>
</div>
<?php
$borrarVehiculoPrivado = new ControllerPrivateVehicle();
$borrarVehiculoPrivado->ctrlDeletePrivateVehicle();
?>