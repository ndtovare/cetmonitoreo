<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar control de pagos
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar control de pagos</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarControlPago">Agregar control de pago</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Pago</th>
              <th style="text-align: center">Total</th>
              <th style="text-align: center">En cuenta</th>
              <th style="text-align: center">Resta</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $controlPagos = ControllerPaymentControl::ctrlShowPaymentControl($item, $valor);
            
            foreach ($controlPagos as $key => $value) {
              echo '
                <tr>
                  <td style="text-align: center">'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["identifier_payment"].'</td>
                  <td style="text-align: center">'.$value["total_payment_control"].'</td>
                  <td style="text-align: center">'.$value["on_account_payment_control"].'</td>
                  <td style="text-align: center">'.$value["substraction_payment_control"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarControlPago" idControlPago="'.$value["id_payment_control"].'" data-toggle="modal" data-target="#modalEditarControlPago"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarControlPago" idControlPago="'.$value["id_payment_control"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarControlPago" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar control de pago</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="nuevoControlPago">
                  <option value="">Seleccionar pago</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $pagos = ControllerPayment::ctrlShowPayments($item, $valor);

                  foreach ($pagos as $key => $value) {
                    echo '<option value="'.$value["id_payment"].'">'.$value["identifier_payment"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoTotal" placeholder="Ingresa el monto total">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoACuenta" placeholder="Ingresa el monto a cuenta">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoResto" placeholder="Ingresa el resto">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar control de pago</button>
        </div>
      </form>
      <?php
      $crearControlPago = new ControllerPaymentControl();
      $crearControlPago->ctrlInsertPaymentControl();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarControlPago" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar control de pago</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="editarControlPago" id="editarControlPago">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $pagos = ControllerPayment::ctrlShowPayments($item, $valor);

                  foreach ($pagos as $key => $value) {
                    echo '<option value="'.$value["id_payment"].'">'.$value["identifier_payment"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="editarTotal" id="editarTotal">
                <input type="hidden" name="idControlPago" id="idControlPago">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="editarACuenta" id="editarACuenta">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" class="form-control input-lg" name="editarResto" id="editarResto">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar control de pago</button>
        </div>
      </form>
      <?php
      $editarControlPago = new ControllerPaymentControl();
      $editarControlPago->ctrlEditPaymentControl();
      ?>
    </div>
  </div>
</div>
<?php
$borrarControlPago = new ControllerPaymentControl();
$borrarControlPago->ctrlDeletePaymentControl();
?>