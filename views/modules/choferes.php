<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar choferes
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar choferes</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Nombres</th>
              <th style="text-align: center">Apellidos</th>
              <th style="text-align: center">Teléfono</th>
              <th style="text-align: center">Tipo de identificación</th>
              <th style="text-align: center">Número de identificación</th>
              <th style="text-align: center">Usuario</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $choferes = ControllerDriver::ctrlShowDrivers($item, $valor);
            
            foreach ($choferes as $key => $value) {
              echo '<tr>
                    <td style="text-align: center">'.($key + 1).'</td>
                    <td style="text-align: center">'.$value["name_owner_driver_client"].'</td>
                    <td style="text-align: center">'.$value["last_name_owner_driver_client"].'</td>
                    <td style="text-align: center">'.$value["phone_owner_driver_client"].'</td>
                    <td style="text-align: center">'.$value["identification_owner_driver_client"].'</td>
                    <td style="text-align: center">'.$value["identification_number_owner_driver_client"].'</td>
                    <td style="text-align: center">'.$value["nickname_user"].'</td>
                    <td style="text-align: center">
                        <div class="btn-group">
                        <button class="btn btn-warning btnEditarChofer" idChofer="'.$value["id_client"].'" data-toggle="modal" data-target="#modalEditarChofer"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btnEliminarChofer" idChofer="'.$value["id_client"].'"><i class="fa fa-times"></i></button>
                        </div>
                    </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalEditarChofer" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar chofer</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreChofer" id="editarNombreChofer">
                <input type="hidden" name="idChofer" id="idChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarApellidoChofer" id="editarApellidoChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="editarTelefonoChofer" id="editarTelefonoChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-child"></i></span>
                <select class="form-control input-lg" name="editarIdentificacionChofer" id="editarIdentificacionChofer">
                  <option value="">Seleccionar identificación</option>
                  <option value="INE">INE</option>
                  <option value="Pasaporte">Pasaporte</option>
                  <option value="Licencia">Licencia</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarNumeroIdentificacionChofer" id="editarNumeroIdentificacionChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" class="form-control input-lg" name="editarDireccionChofer" id="editarDireccionChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarNombreReferenciaChofer" id="editarNombreReferenciaChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control input-lg" name="editarApellidoReferenciaChofer" id="editarApellidoReferenciaChofer">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="number" class="form-control input-lg" name="editarTelefonoReferenciaChofer" id="editarTelefonoReferenciaChofer">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar chofer</button>
        </div>
      </form>
      <?php
      $editarChofer = new ControllerDriver();
      $editarChofer->ctrlEditDriver();
      ?>
    </div>
  </div>
</div>
<?php
$borrarChofer = new ControllerDriver();
$borrarChofer->ctrlDeleteDriver();
?>