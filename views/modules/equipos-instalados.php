<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar equipos instalados
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar equipos instalados</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
        <div class="box-header with-border">
        <button class="btn btn-info" data-toggle="modal" data-target="#modalVerEquiposInstalados">Ver equipos instalados</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Foto</th>
              <th style="text-align: center">Nombre del producto</th>
              <th style="text-align: center">Factura</th>
              <th style="text-align: center">Número de serie</th>
              <th style="text-align: center">Estatus</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $productos = ControllerProduct::ctrlShowProducts($item, $valor, 1);
            
            foreach ($productos as $key => $value) {
              $fechaAdmisionEditada = $value["date_admission_lot"];
              $timeStampAdmision = strtotime($fechaAdmisionEditada);
              $fechaAdmisionOriginal = date("d/m/Y", $timeStampAdmision);
              echo '
                <tr>
                  <td style="text-align: center">'.($key + 1).'</td>';
              if($value["image_code"] != "") {
                echo '<td style="text-align: center"><img src="'.$value["image_code"].'" class="img-thumbnail" width="60px;"></td>';
              } else {
                echo '<td style="text-align: center"><img src="views/img/productos/default/anonymous.png" class="img-thumbnail" width="60px;"></td>';
              }
              echo '
                  <td style="text-align: center">'.$value["name_code"].'</td>
                  <td style="text-align: center">'.$value["bill_lot"].'</td>
                  <td style="text-align: center">'.$value["serial_number_product"].'</td>
                  <td style="text-align: center">'.$value["status_product"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-primary btnAgregarEquipoInstalado" idEquipoInstalado="'.$value["id_product"].'" data-toggle="modal" data-target="#modalAgregarEquipoInstalado">Agregar</button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarEquipoInstalado" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar equipo instalado</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                <input type="text" class="form-control input-lg" name="nuevaDescripción" placeholder="Ingresa la descripción">
                <input type="hidden" name="nuevoLote" id="nuevoLote">
                <input type="hidden" name="nuevoCodigo" id="nuevoCodigo">
                <input type="hidden" name="nuevoVehiculoPrivado" id="nuevoVehiculoPrivado">
                <input type="hidden" name="nuevoTransportePublico" id="nuevoTransportePublico">
                <input type="hidden" name="idProducto" id="idProducto">
                <input type="hidden" name="nuevoSerieProducto" id="nuevoSerieProducto">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoComentario" placeholder="Ingresa los comentarios">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right datepicker" name="nuevaFechaInstalacion" placeholder="Ingresa la fecha de instalación">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="seleccionaVehiculoEquipoInstalado" id="seleccionaVehiculoEquipoInstalado">
                  <option value="">Selecciona un tipo de vehículo</option>
                  <option value="TP">Transporte público</option>
                  <option value="VP">Vehículo privado</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="nuevoTransportePublicoEquipoInstalado">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="nuevoTransportePublico">
                  <option value="">Seleccionar transporte público</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="nuevoVehiculoPrivadoEquipoInstalado">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="nuevoVehiculoPrivado">
                  <option value="">Seleccionar vehículo privado</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar equipo instalado</button>
        </div>
      </form>
      <?php
      $crearEquipoInstalado = new ControllerInstalledEquipment();
      $crearEquipoInstalado->ctrlInsertInstalledEquipment();
      ?>
    </div>
  </div>
</div>
<div id="modalVerEquiposInstalados" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Equipos instalados</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
          <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Foto</th>
              <th style="text-align: center">Descripción</th>
              <th style="text-align: center">Factura</th>
              <th style="text-align: center">Fecha de Instalación</th>
              <th style="text-align: center">Descripción</th>
              <th style="text-align: center">Comentarios</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $equiposInstalados = ControllerInstalledEquipment::ctrlShowInstalledEquipment($item, $valor);
            
            foreach ($equiposInstalados as $key => $value) {
              $fechaInstalacionEditada = $value["installation_date_installed_equipment"];
              $timeStampInstalacion = strtotime($fechaInstalacionEditada);
              $fechaInstalacionOriginal = date("d/m/Y", $timeStampInstalacion);
              echo '
                <tr>
                  <td>'.($key + 1).'</td>';
              if($value["image_code"] != "") {
                echo '<td style="text-align: center"><img src="'.$value["image_code"].'" class="img-thumbnail" width="60px;"></td>';
              } else {
                echo '<td style="text-align: center"><img src="views/img/productos/default/anonymous.png" class="img-thumbnail" width="60px;"></td>';
              }
              echo '
                  <td style="text-align: center">'.$value["description_code"].'</td>
                  <td style="text-align: center">'.$value["bill_lot"].'</td>
                  <td style="text-align: center">'.$fechaInstalacionOriginal.'</td>
                  <td style="text-align: center">'.$value["description_installed_equipment"].'</td>
                  <td style="text-align: center">'.$value["comments_installed_equipment"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                    <button class="btn btn-warning btnEditarEquipoInstalado" idEquipoInstalado="'.$value["id_installed_equipment"].'" data-toggle="modal" data-target="#modalEditarEquipoInstalado"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btnEliminarEquipoInstalado" idEquipoInstalado="'.$value["id_installed_equipment"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
          </div>
        </div>
    </div>
  </div>
</div>
<div id="modalEditarEquipoInstalado" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar equipo instalado</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                <input type="text" class="form-control input-lg" name="editarDescripción" id="editarDescripción">
                <input type="hidden" name="idEquipoInstalado" id="idEquipoInstalado">
                <input type="hidden" name="editarLote" id="editarLote">
                <input type="hidden" name="editarCodigo" id="editarCodigo">
                <input type="hidden" name="editarSeleccionaVehiculoEI" id="editarSeleccionaVehiculoEI">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                <input type="text" class="form-control input-lg" name="editarComentario" id="editarComentario">
              </div>
            </div>
            <div class="form-group">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" name="editarFechaInstalacion" id="editarFechaInstalacion">
                </div>
            </div>
            <div class="form-group" id="editarTransportePublicoEquipoInstalado">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="editarTransportePublico" id="editarTransportePublico">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="editarVehiculoPrivadoEquipoInstalado">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="editarVehiculoPrivado" id="editarVehiculoPrivado">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar equipo instalado</button>
        </div>
      </form>
      <?php
      $editarEquipoInstalado = new ControllerInstalledEquipment();
      $editarEquipoInstalado->ctrlEditInstalledEquipment();
      ?>
    </div>
  </div>
</div>
<?php
$borrarEquipoInstalado = new ControllerInstalledEquipment();
$borrarEquipoInstalado->ctrlDeleteInstalledEquipment();
?>