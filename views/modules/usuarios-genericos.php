<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar usuarios genéricos
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar usuarios genéricos</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuarioGenerico">Agregar usuario genérico</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Contraseña</th>
              <th style="text-align: center">Identificador de vehículo</th>
              <th style="text-align: center">Tipo de vehículo</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $usuarioGenerico = ControllerGenericUser::ctrlShowGenericUsers($item, $valor);
            
            foreach ($usuarioGenerico as $key => $value) {
              echo '
                <tr>
                  <td>'.($key + 1).'</td>
                  <td style="text-align: center">'.$value["password_generic_user"].'</td>
                  <td style="text-align: center">'.$value["identifier_private_public_vehicle"].'</td>
                  <td style="text-align: center">'.$value["type_vehicle"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarUsuarioGenerico" idUsuarioGenerico="'.$value["id_generic_user"].'" data-toggle="modal" data-target="#modalEditarUsuarioGenerico"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarUsuarioGenerico" idUsuarioGenerico="'.$value["id_generic_user"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarUsuarioGenerico" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar usuario genérico</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="text" class="form-control input-lg" name="nuevaContrasenia" placeholder="Ingresa la contraseña">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="seleccionaVehiculoGenerico" id="seleccionaVehiculoGenerico">
                  <option value="">Selecciona un tipo de vehículo</option>
                  <option value="TP">Transporte público</option>
                  <option value="VP">Vehículo privado</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="nuevoTransportePublicoGenerico">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="nuevoTransportePublico">
                  <option value="">Seleccionar transporte público</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="nuevoVehiculoPrivadoGenerico">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="nuevoVehiculoPrivado">
                  <option value="">Seleccionar vehículo privado</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar usuario genérico</button>
        </div>
      </form>
      <?php
      $crearUsuarioGenerico = new ControllerGenericUser();
      $crearUsuarioGenerico->ctrlInsertGenericUser();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarUsuarioGenerico" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar usuario genérico</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="text" class="form-control input-lg" name="editarContrasenia" id="editarContrasenia">
                <input type="hidden" name="idUsuarioGenerico" id="idUsuarioGenerico">
                <input type="hidden" name="editarSeleccionaVehiculoG" id="editarSeleccionaVehiculoG">
              </div>
            </div>
            <div class="form-group" id="editarTransportePublicoGenerico">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                <select class="form-control input-lg" name="editarTransportePublico" id="editarTransportePublico">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $transportesPublicos = ControllerPublicTransport::ctrlShowPublicTransports($item, $valor);

                  foreach ($transportesPublicos as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="editarVehiculoPrivadoGenerico">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                <select class="form-control input-lg" name="editarVehiculoPrivado" id="editarVehiculoPrivado">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $vehiculosPrivados = ControllerPrivateVehicle::ctrlShowPrivateVehicles($item, $valor);

                  foreach ($vehiculosPrivados as $key => $value) {
                    echo '<option value="'.$value["id_vehicle"].'">'.$value["identifier_private_public_vehicle"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar usuario genérico</button>
        </div>
      </form>
      <?php
      $editarUsuarioGenerico = new ControllerGenericUser();
      $editarUsuarioGenerico->ctrlEditGenericUser();
      ?>
    </div>
  </div>
</div>
<?php
$borrarUsuarioGenerico = new ControllerGenericUser();
$borrarUsuarioGenerico->ctrlDeleteGenericUser();
?>