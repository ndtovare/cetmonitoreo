<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Administrar productos
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar productos</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProducto">Agregar producto</button>
      </div>
      <div class="box-body">
        <table class="table table-bordered dt-responsive tabla">
          <thead>
            <tr>
              <th style="width: 10px;">#</th>
              <th style="text-align: center">Foto</th>
              <th style="text-align: center">Nombre</th>
              <th style="text-align: center">Factura</th>
              <th style="text-align: center">Fecha de admisión</th>
              <th style="text-align: center">Estatus</th>
              <th style="text-align: center">Número de serie</th>
              <th style="text-align: center">Usuario</th>
              <th style="text-align: center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valor = null;
            $productos = ControllerProduct::ctrlShowProducts($item, $valor, 0);
            
            foreach ($productos as $key => $value) {
              $fechaAdmisionEditada = $value["date_admission_lot"];
              $timeStampAdmision = strtotime($fechaAdmisionEditada);
              $fechaAdmisionOriginal = date("d/m/Y", $timeStampAdmision);
              echo '
                <tr>
                  <td>'.($key + 1).'</td>';
              if($value["image_code"] != "") {
                echo '<td style="text-align: center"><img src="'.$value["image_code"].'" class="img-thumbnail" width="60px;"></td>';
              } else {
                echo '<td style="text-align: center"><img src="views/img/productos/default/anonymous.png" class="img-thumbnail" width="60px;"></td>';
              }
              echo '
                  <td style="text-align: center">'.$value["name_code"].'</td>
                  <td style="text-align: center">'.$value["bill_lot"].'</td>
                  <td style="text-align: center">'.$fechaAdmisionOriginal.'</td>
                  <td style="text-align: center">'.$value["status_product"].'</td>
                  <td style="text-align: center">'.$value["serial_number_product"].'</td>
                  <td style="text-align: center">'.$value["nickname_user"].'</td>
                  <td style="text-align: center">
                    <div class="btn-group">
                      <button class="btn btn-warning btnEditarProducto" idProducto="'.$value["id_product"].'" data-toggle="modal" data-target="#modalEditarProducto"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btnEliminarProducto" idProducto="'.$value["id_product"].'"><i class="fa fa-times"></i></button>
                    </div>
                  </td>
                </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
<div id="modalAgregarProducto" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar producto</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoNumeroSerie" placeholder="Ingresa el número de serie">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                <select class="form-control input-lg" name="nuevoEstatus">
                  <option value="0">Seleccionar estatus</option>
                  <option value="Instalado">Instalado</option>
                  <option value="Disponible">Disponible</option>
                  <option value="Garantía">Garantía</option>
                  <option value="Sin usar">Sin usar</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="nuevoCodigo">
                  <option value="0">Seleccionar código</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $codigos = ControllerCode::ctrlShowCodes($item, $valor);

                  foreach ($codigos as $key => $value) {
                    echo '<option value="'.$value["id_code"].'">'.$value["name_code"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                <select class="form-control input-lg" name="nuevoLote">
                  <option value="0">Seleccionar lote</option>
                  <?php
                  $item = null;
                  $valor = null;
                  $lotes = ControllerLot::ctrlShowLots($item, $valor);

                  foreach ($lotes as $key => $value) {
                    echo '<option value="'.$value["id_lot"].'">'.$value["bill_lot"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar producto</button>
        </div>
      </form>
      <?php
      $crearProducto = new ControllerProduct();
      $crearProducto->ctrlInsertProduct();
      ?>
    </div>
  </div>
</div>
<div id="modalEditarProducto" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar producto</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input type="text" class="form-control input-lg" name="editarNumeroSerie" id="editarNumeroSerie">
                <input type="hidden" name="idProducto" id="idProducto">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                <select class="form-control input-lg" name="editarEstatus" id="editarEstatus">
                  <option></option>
                  <option value="Instalado">Instalado</option>
                  <option value="Disponible">Disponible</option>
                  <option value="Garantía">Garantía</option>
                  <option value="Sin usar">Sin usar</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <select class="form-control input-lg" name="editarCodigo" id="editarCodigo">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $codigos = ControllerCode::ctrlShowCodes($item, $valor);

                  foreach ($codigos as $key => $value) {
                    echo '<option value="'.$value["id_code"].'">'.$value["name_code"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                <select class="form-control input-lg" name="editarLote" id="editarLote">
                  <option></option>
                  <?php
                  $item = null;
                  $valor = null;
                  $lotes = ControllerLot::ctrlShowLots($item, $valor);

                  foreach ($lotes as $key => $value) {
                    echo '<option value="'.$value["id_lot"].'">'.$value["bill_lot"].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Editar producto</button>
        </div>
      </form>
      <?php
      $editarProducto = new ControllerProduct();
      $editarProducto->ctrlEditProduct();
      ?>
    </div>
  </div>
</div>
<?php
$borrarProducto = new ControllerProduct();
$borrarProducto->ctrlDeleteProduct();
?>