<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar vehículos</h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar vehiculos</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <a href="transportes-publicos" class="btn btn-primary">Ver transportes públicos</a>
                <a href="vehiculos-privados" class="btn btn-primary">Ver vehículos privados</a>
            </div>
            <div class="box-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="modal-header" style="background: #3c8dbc; color: white;">
                        <h4 class="modal-title">Agregar vehículo</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-body">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                                            <select class="form-control input-lg" name="nuevoTipoVehiculo" id="nuevoTipoVehiculo">
                                                <option value="">Seleccionar tipo de vehículo</option>
                                                <option value="Transporte público">Transporte público</option>
                                                <option value="Vehículo privado">Vehículo privado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoImeiVehiculo" placeholder="Ingresa el IMEI">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoIdentificadorVehiculo" placeholder="Ingresa el identificador">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevaMarcaVehiculo" placeholder="Ingresa la marca">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-gear"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoModeloVehiculo" placeholder="Ingresa el modelo">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoAnioVehiculo" placeholder="Ingresa el año">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevaSerieVehiculo" placeholder="Ingresa el número de serie">
                                        </div>
                                    </div>
                                    <div class="form-group" id="nuevoEconomicoVehiculo">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoEconomicoVehiculo" placeholder="Ingresa el número económico">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevaPlacaVehiculo" placeholder="Ingresa el número de placa">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                                            <select class="form-control input-lg" name="nuevaClaseVehiculo">
                                                <option value="">Seleccionar clase de vehículo</option>
                                                <option value="Autobús">Autobús</option>
                                                <option value="Sedán">Sedán</option>
                                                <option value="Vagoneta">Vagoneta</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                            <select class="form-control input-lg" name="nuevoPropietario">
                                                <option value="">Seleccionar propietario</option>
                                                <?php
                                                $item = null;
                                                $valor = null;
                                                $propietarios = ControllerOwner::ctrlShowOwners($item, $valor);

                                                foreach ($propietarios as $key => $value) {
                                                    echo '<option value="'.$value["id_owner"].'">'.$value["name_owner"].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                            <select class="form-control input-lg" name="nuevoChofer">
                                                <option value="">Seleccionar chófer</option>
                                                <?php
                                                $item = null;
                                                $valor = null;
                                                $choferes = ControllerDriver::ctrlShowDrivers($item, $valor);

                                                foreach ($choferes as $key => $value) {
                                                    echo '<option value="'.$value["id_driver"].'">'.$value["name_driver"].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-industry"></i></span>
                                            <select class="form-control input-lg" name="nuevaCompania">
                                                <option value="">Seleccionar compañía</option>
                                                <?php
                                                $item = null;
                                                $valor = null;
                                                $compania = ControllerCompany::ctrlShowCompanies($item, $valor);

                                                foreach ($compania as $key => $value) {
                                                    echo '<option value="'.$value["id_business"].'">'.$value["name_business"].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar vehículo</button>
                    </div>
                </form>
                <?php
                $crearVehicle = new ControllerVehicle();
                $crearVehicle->ctrlInsertVehicle();
                ?>
            </div>
        </div>
    </section>
</div>
