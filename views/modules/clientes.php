<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar clientes</h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar clientes</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <a href="propietarios" class="btn btn-primary">Ver propietarios</a>
                <a href="choferes" class="btn btn-primary">Ver choferes</a>
            </div>
            <div class="box-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="modal-header" style="background: #3c8dbc; color: white;">
                        <h4 class="modal-title">Agregar cliente</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="modal-body">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bus"></i></span>
                                            <select class="form-control input-lg" name="nuevoTipoCliente" id="nuevoTipoCliente">
                                                <option value="">Seleccionar tipo de cliente</option>
                                                <option value="Propietario">Propietario</option>
                                                <option value="Chofer">Chofer</option>
                                                <option value="Propietario - chofer">Propietario - chofer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoNombreCliente" placeholder="Ingresa los nombres">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoApellidoCliente" placeholder="Ingresa los apellidos">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            <input type="number" class="form-control input-lg" name="nuevoTelefonoCliente" placeholder="Ingresa el teléfono">
                                        </div>
                                    </div>
                                    <div class="form-group" id="nuevoTelefonoFijoCliente">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            <input type="number" class="form-control input-lg" name="nuevoTelefonoFijoCliente"  placeholder="Ingresa el teléfono fijo ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-child"></i></span>
                                            <select class="form-control input-lg" name="nuevaIdentificacionCliente">
                                                <option value="">Seleccionar identificación</option>
                                                <option value="INE">INE</option>
                                                <option value="Pasaporte">Pasaporte</option>
                                                <option value="Licencia">Licencia</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoNumeroIdentificacionCliente" placeholder="Ingresa el número de identificación">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevaDireccionCliente" placeholder="Ingresa la dirección">
                                        </div>
                                    </div>
                                    <div class="form-group" id="nuevoCorreoCliente">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            <input type="email" class="form-control input-lg" name="nuevoCorreoCliente"  placeholder="Ingresa el correo electrónico">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoNombreReferenciaCliente" placeholder="Ingresa los nombres de la referencia">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" class="form-control input-lg" name="nuevoApellidoReferenciaCliente" placeholder="Ingresa los apellidos de la referencia">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            <input type="number" class="form-control input-lg" name="nuevoTelefonoReferenciaCliente" placeholder="Ingresa el teléfono de la referencia">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Guardar cliente</button>
                    </div>
                </form>
                <?php
                $crearClient = new ControllerClient();
                $crearClient->ctrlInsertClient();
                ?>
            </div>
        </div>
    </section>
</div>