<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CETMonitoreo</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="views/img/plantilla/cet-icono.png" type="image/x-icon">
        <link rel="stylesheet" href="views/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="views/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="views/bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="views/dist/css/AdminLTE.css">
        <link rel="stylesheet" href="views/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="views/bower_components/datatables.net-bs/css/datatables.bootstrap.min.css">
        <link rel="stylesheet" href="views/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">
        <link rel="stylesheet" href="views/plugins/iCheck/all.css">
        <link rel="stylesheet" href="views/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="views/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="views/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="views/bower_components/fastclick/lib/fastclick.js"></script>
        <script src="views/dist/js/adminlte.min.js"></script>
        <script src="views/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="views/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="views/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
        <script src="views/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>
        <script src="views/plugins/SweetAlert2/sweetalert2.all.js"></script>
        <script src="views/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-collapse sidebar-mini login-page">
        <?php
        if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {
            echo '<div class="wrapper">';

            include "modules/cabezote.php"; 
            include "modules/menu.php"; 
    
            if(isset($_GET["ruta"])) {
                if($_GET["ruta"] == "inicio" ||
                    $_GET["ruta"] == "usuarios" ||
                    $_GET["ruta"] == "productos" ||
                    $_GET["ruta"] == "lotes" ||
                    $_GET["ruta"] == "codigos" ||
                    $_GET["ruta"] == "propietarios" ||
                    $_GET["ruta"] == "choferes" ||
                    $_GET["ruta"] == "transportes-publicos" ||
                    $_GET["ruta"] == "vehiculos-privados" ||
                    $_GET["ruta"] == "companias" ||
                    $_GET["ruta"] == "flotillas" ||
                    $_GET["ruta"] == "usuarios-genericos" ||
                    $_GET["ruta"] == "sims" ||
                    $_GET["ruta"] == "pagos" ||
                    $_GET["ruta"] == "control-pagos" ||
                    $_GET["ruta"] == "equipos-instalados" ||
                    $_GET["ruta"] == "revisiones" ||
                    $_GET["ruta"] == "vehiculos" ||
                    $_GET["ruta"] == "clientes" ||
                    $_GET["ruta"] == "salir") {
                    include "modules/".$_GET["ruta"].".php"; 
                } else {
                    include "modules/404.php";
                }
            } else {
                include "modules/inicio.php";
            }
            
            include "modules/footer.php";
    
            echo '</div>';
        } else {
            include "modules/login.php";
        }
        
        ?>
        <script src="views/js/plantilla.js"></script>
        <script src="views/js/usuarios.js"></script>
        <script src="views/js/codigos.js"></script>
        <script src="views/js/lotes.js"></script>
        <script src="views/js/productos.js"></script>
        <script src="views/js/propietarios.js"></script>
        <script src="views/js/choferes.js"></script>
        <script src="views/js/companias.js"></script>
        <script src="views/js/flotillas.js"></script>
        <script src="views/js/usuarios-genericos.js"></script>
        <script src="views/js/sims.js"></script>
        <script src="views/js/pagos.js"></script>
        <script src="views/js/control-pagos.js"></script>
        <script src="views/js/transportes-publicos.js"></script>
        <script src="views/js/vehiculos-privados.js"></script>
        <script src="views/js/equipos-instalados.js"></script>
        <script src="views/js/revisiones.js"></script>
        <script src="views/js/vehiculos.js"></script>
        <script src="views/js/clientes.js"></script>
    </body>
</html>
