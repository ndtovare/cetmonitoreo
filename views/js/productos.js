$(document).on("click", ".btnEditarProducto", function() {
    var idProducto = $(this).attr("idProducto");
    var datos = new FormData();
    datos.append("idProducto", idProducto);

    $.ajax({
        url: "ajax/productos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            console.log(respuesta);
            $("#editarNumeroSerie").val(respuesta["serial_number_product"]);
            $("#editarEstatus").val(respuesta["status_product"]);
            $("#editarCodigo").val(respuesta["id_code"]);
            $("#editarLote").val(respuesta["id_lot"]);
            $("#idProducto").val(respuesta["id_product"]);
        }
    });
});

$(document).on("click", ".btnEliminarProducto", function() {
    var idProducto = $(this).attr("idProducto");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el producto?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar producto!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=productos&idProducto=" + idProducto;
        }
    });
});