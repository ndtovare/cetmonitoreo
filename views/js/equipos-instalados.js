var publicoEquipoInstalado = $('#nuevoTransportePublicoEquipoInstalado');
var privadoEquipoInstalado = $('#nuevoVehiculoPrivadoEquipoInstalado');
var editarpublicoEquipoInstalado = $('#editarTransportePublicoEquipoInstalado');
var editarprivadoEquipoInstalado = $('#editarVehiculoPrivadoEquipoInstalado');

$(document).ready(function() {
    publicoEquipoInstalado.hide();
    privadoEquipoInstalado.hide();
    editarpublicoEquipoInstalado.hide();
    editarprivadoEquipoInstalado.hide();
});

$(document).on("click", ".btnAgregarEquipoInstalado", function() {
    var idEquipoInstalado = $(this).attr("idEquipoInstalado");
    var datos = new FormData();
    datos.append("idEquipoInstalado", idEquipoInstalado);

    $.ajax({
        url: "ajax/productos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            console.log(respuesta);
            $("#nuevaImagen").val(respuesta["image_code"]);
            $("#nuevoLote").val(respuesta["id_lot"]);
            $("#nuevoCodigo").val(respuesta["id_code"]);
            $("#nuevoVehiculoPrivado").val(respuesta["id_private_vehicle"]);
            $("#nuevoTransportePublico").val(respuesta["id_public_transport"]);
            $("#idProducto").val(respuesta["id_product"]);
            $("#nuevoSerieProducto").val(respuesta["serial_number_product"]);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

$(document).on("click", ".btnEditarEquipoInstalado", function() {
    var idEquipoInstalado = $(this).attr("idEquipoInstalado");
    var datos = new FormData();
    datos.append("idEquipoInstalado", idEquipoInstalado);

    $.ajax({
        url: "ajax/equipos-instalados.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            var editarVehiculoSeleccionado = $("#editarSeleccionaVehiculoEI");
            console.log(editarVehiculoSeleccionado);

            if(respuesta["type_vehicle"] == "Transporte público") {
                editarVehiculoSeleccionado.val("TP");
            } else if(respuesta["type_vehicle"] == "Vehículo privado") {
                editarVehiculoSeleccionado.val("VP");
            }

            if(editarVehiculoSeleccionado.val() == "TP") {
                editarpublicoEquipoInstalado.show();
                editarprivadoEquipoInstalado.hide();
                $("#editarTransportePublico").val(respuesta["id_vehicle"]);
            } else if(editarVehiculoSeleccionado.val() == "VP") {
                editarprivadoEquipoInstalado.show();
                editarpublicoEquipoInstalado.hide();
                $("#editarVehiculoPrivado").val(respuesta["id_vehicle"]);
            }

            $("#editarDescripción").val(respuesta["description_installed_equipment"]);
            $("#editarComentario").val(respuesta["comments_installed_equipment"]);
            $("#editarFechaInstalacion").val(respuesta["installation_date_installed_equipment"]);
            $("#editarLote").val(respuesta["id_lot"]);
            $("#editarCodigo").val(respuesta["id_code"]);
            $("#idEquipoInstalado").val(respuesta["id_installed_equipment"]);
        }
    });
});

$(document).on("click", ".btnEliminarEquipoInstalado", function() {
    var idEquipoInstalado = $(this).attr("idEquipoInstalado");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el equipo instalado?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar equipo instalado!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=equipos-instalados&idEquipoInstalado=" + idEquipoInstalado;
        }
    });
});

$(document).on("change", "#seleccionaVehiculoEquipoInstalado", function() {
    var vehiculoSeleccionado = $('#seleccionaVehiculoEquipoInstalado').val();
    console.log(vehiculoSeleccionado);
    if(vehiculoSeleccionado == "TP") {
        publicoEquipoInstalado.show();
        privadoEquipoInstalado.hide();
    } else if(vehiculoSeleccionado == "VP") {
        privadoEquipoInstalado.show();
        publicoEquipoInstalado.hide();
    }
});