$(document).on("click", ".btnEditarSim", function() {
    var idSim = $(this).attr("idSim");
    var datos = new FormData();
    datos.append("idSim", idSim);

    $.ajax({
        url: "ajax/sims.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#idSim").val(respuesta["id_sim"]);
            $("#editarEstatus").val(respuesta["status_sim"]);
            $("#editarNumeroSerie").val(respuesta["serial_number_sim"]);
            $("#editarNumeroSim").val(respuesta["number_sim"]);
            $("#editarEsquema").val(respuesta["scheme_sim"]);
            $("#editarSaldo").val(respuesta["balance_sim"]);
            $("#editarFecha").val(respuesta["date_sim"]);
            $("#editarFechaCorte").val(respuesta["cutting_day_sim"]);
            $("#editarCodigo").val(respuesta["id_code"]);
            $("#editarLote").val(respuesta["id_lot"]);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

$(document).on("click", ".btnEliminarSim", function() {
    var idSim = $(this).attr("idSim");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el sim?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar sim!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=sims&idSim=" + idSim;
        }
    });
});