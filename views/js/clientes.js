var telefonoFijo = $('#nuevoTelefonoFijoCliente');
var correo = $('#nuevoCorreoCliente');

$(document).ready(function() {
    telefonoFijo.show();
    correo.show();
    correo.val('');
});

$(document).on("change", "#nuevoTipoCliente", function() {
    var nuevoCliente = $('#nuevoTipoCliente').val();
    if(nuevoCliente == "Propietario" || nuevoCliente == "Propietario - chofer") {
        telefonoFijo.show();
        correo.show();
    } else if(nuevoCliente == "Chofer") {
        telefonoFijo.hide();
        correo.hide();
    } else {
        telefonoFijo.hide();
        correo.hide();
    }
});