var publicoGenerico = $('#nuevoTransportePublicoGenerico');
var privadoGenerico = $('#nuevoVehiculoPrivadoGenerico');
var editarpublicoGenerico = $('#editarTransportePublicoGenerico');
var editarprivadoGenerico = $('#editarVehiculoPrivadoGenerico');

$(document).ready(function() {
    publicoGenerico.hide();
    privadoGenerico.hide();
    editarpublicoGenerico.hide();
    editarprivadoGenerico.hide();
});

$(document).on("click", ".btnEditarUsuarioGenerico", function() {
    var idUsuarioGenerico = $(this).attr("idUsuarioGenerico");
    var datos = new FormData();
    datos.append("idUsuarioGenerico", idUsuarioGenerico);

    $.ajax({
        url: "ajax/usuarios-genericos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            var editarVehiculoSeleccionado = $("#editarSeleccionaVehiculoG");
            console.log(editarVehiculoSeleccionado);

            if(respuesta["type_vehicle"] == "Transporte público") {
                editarVehiculoSeleccionado.val("TP");
            } else if(respuesta["type_vehicle"] == "Vehículo privado") {
                editarVehiculoSeleccionado.val("VP");
            }

            if(editarVehiculoSeleccionado.val() == "TP") {
                editarpublicoGenerico.show();
                editarprivadoGenerico.hide();
                $("#editarTransportePublico").val(respuesta["id_vehicle"]);
            } else if(editarVehiculoSeleccionado.val() == "VP") {
                editarprivadoGenerico.show();
                editarpublicoGenerico.hide();
                $("#editarVehiculoPrivado").val(respuesta["id_vehicle"]);
            }

            $("#editarContrasenia").val(respuesta["password_generic_user"]);
            $("#idUsuarioGenerico").val(respuesta["id_generic_user"]);
        }
    });
});

$(document).on("click", ".btnEliminarUsuarioGenerico", function() {
    var idUsuarioGenerico = $(this).attr("idUsuarioGenerico");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el usuario genérico?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar usuario genérico!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=usuarios-genericos&idUsuarioGenerico=" + idUsuarioGenerico;
        }
    });
});

$(document).on("change", "#seleccionaVehiculoGenerico", function() {
    var vehiculoSeleccionado = $('#seleccionaVehiculoGenerico').val();
    console.log(vehiculoSeleccionado);
    if(vehiculoSeleccionado == "TP") {
        publicoGenerico.show();
        privadoGenerico.hide();
    } else if(vehiculoSeleccionado == "VP") {
        privadoGenerico.show();
        publicoGenerico.hide();
    }
});