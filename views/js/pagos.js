var publicoPago = $('#nuevoTransportePublicoPago');
var privadoPago = $('#nuevoVehiculoPrivadoPago');
var editarpublicoPago = $('#editarTransportePublicoPago');
var editarprivadoPago = $('#editarVehiculoPrivadoPago');

$(document).ready(function() {
    publicoPago.hide();
    privadoPago.hide();
    editarpublicoPago.hide();
    editarprivadoPago.hide();
});

$(document).on("click", ".btnEditarPago", function() {
    var idPago = $(this).attr("idPago");
    var datos = new FormData();
    datos.append("idPago", idPago);

    $.ajax({
        url: "ajax/pagos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            var editarVehiculoSeleccionado = $("#editarSeleccionaVehiculoP");
            console.log(editarVehiculoSeleccionado);

            if(respuesta["type_vehicle"] == "Transporte público") {
                editarVehiculoSeleccionado.val("TP");
            } else if(respuesta["type_vehicle"] == "Vehículo privado") {
                editarVehiculoSeleccionado.val("VP");
            }

            if(editarVehiculoSeleccionado.val() == "TP") {
                editarpublicoPago.show();
                editarprivadoPago.hide();
                $("#editarTransportePublico").val(respuesta["id_vehicle"]);
            } else if(editarVehiculoSeleccionado.val() == "VP") {
                editarprivadoPago.show();
                editarpublicoPago.hide();
                $("#editarVehiculoPrivado").val(respuesta["id_vehicle"]);
            }

            $("#idPago").val(respuesta["id_payment"]);
            $("#editarIdentificadorPago").val(respuesta["identifier_payment"]);
            $("#editarImeiPago").val(respuesta["imei_payment"]);
            $("#editarMetodoPago").val(respuesta["method_payment"]);
            $("#editarNumeroPago").val(respuesta["number_payment"]);
            $("#editarFechaPago").val(respuesta["date_payment"]);
            $("#editarMontoPago").val(respuesta["amount_payment"]);
            $("#editarCompania").val(respuesta["id_business"]);
            $("#editarPropietario").val(respuesta["id_owner"]);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

$(document).on("click", ".btnEliminarPago", function() {
    var idPago = $(this).attr("idPago");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el pago?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar pago!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=pagos&idPago=" + idPago;
        }
    });
});

$(document).on("change", "#seleccionaVehiculoPago", function() {
    var vehiculoSeleccionado = $('#seleccionaVehiculoPago').val();
    console.log(vehiculoSeleccionado);
    if(vehiculoSeleccionado == "TP") {
        publicoPago.show();
        privadoPago.hide();
    } else if(vehiculoSeleccionado == "VP") {
        privadoPago.show();
        publicoPago.hide();
    }
});