$(document).on("click", ".btnEditarVehiculoPrivado", function() {
    var idVehiculoPrivado = $(this).attr("idVehiculoPrivado");
    var datos = new FormData();
    datos.append("idVehiculoPrivado", idVehiculoPrivado);

    $.ajax({
        url: "ajax/vehiculos-privados.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#editarImeiTP").val(respuesta["imei_private_public_vehicle"]);
            $("#editarIdentificadorTP").val(respuesta["identifier_private_public_vehicle"]);
            $("#editarMarcaTP").val(respuesta["brand_private_public_vehicle"]);
            $("#editarModeloTP").val(respuesta["model_private_public_vehicle"]);
            $("#editarAnioTP").val(respuesta["year_private_public_vehicle"]);
            $("#editarSerieTP").val(respuesta["vehicular_series_private_public_vehicle"]);
            $("#editarPlacaTP").val(respuesta["plate_number_private_public_vehicle"]);
            $("#editarTipoTP").val(respuesta["class_private_public_vehicle"]);
            $("#editarPropietario").val(respuesta["id_owner"]);
            $("#editarChofer").val(respuesta["id_driver"]);
            $("#idVehiculoPrivado").val(respuesta["id_vehicle"]);
        }
    });
});

$(document).on("click", ".btnEliminarVehiculoPrivado", function() {
    var idVehiculoPrivado = $(this).attr("idVehiculoPrivado");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el vehículo privado?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar vehículo privado!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=vehiculos-privados&idVehiculoPrivado=" + idVehiculoPrivado;
        }
    });
});