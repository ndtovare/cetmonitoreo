$(document).on("click", ".btnEditarPropietario", function() {
    var idPropietario = $(this).attr("idPropietario");
    var datos = new FormData();
    datos.append("idPropietario", idPropietario);
    console.log(idPropietario);

    $.ajax({
        url: "ajax/propietarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            console.log(respuesta);
            $("#editarNombrePropietario").val(respuesta["name_owner_driver_client"]);
            $("#editarApellidoPropietario").val(respuesta["last_name_owner_driver_client"]);
            $("#editarTelefonoPropietario").val(respuesta["phone_owner_driver_client"]);
            $("#editarTelefonoFijoPropietario").val(respuesta["landline_owner_client"]);
            $("#editarIdentificacionPropietario").val(respuesta["identification_owner_driver_client"]);
            $("#editarNumeroIdentificacionPropietario").val(respuesta["identification_number_owner_driver_client"]);
            $("#editarDireccionPropietario").val(respuesta["address_owner_driver_client"]);
            $("#editarCorreoPropietario").val(respuesta["email_owner_client"]);
            $("#editarNombreReferenciaPropietario").val(respuesta["reference_name_owner_driver_client"]);
            $("#editarApellidoReferenciaPropietario").val(respuesta["reference_last_name_owner_driver_client"]);
            $("#editarTelefonoReferenciaPropietario").val(respuesta["reference_phone_owner_driver_client"]);
            $("#idPropietario").val(respuesta["id_client"]);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

$(document).on("click", ".btnEliminarPropietario", function() {
    var idPropietario = $(this).attr("idPropietario");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el propietario?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar propietario!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=propietarios&idPropietario=" + idPropietario;
        }
    });
});