$(document).on("click", ".btnEditarControlPago", function() {
    var idControlPago = $(this).attr("idControlPago");
    var datos = new FormData();
    datos.append("idControlPago", idControlPago);

    $.ajax({
        url: "ajax/control-pagos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#editarControlPago").val(respuesta["id_payment"]);
            $("#editarTotal").val(respuesta["total_payment_control"]);
            $("#editarACuenta").val(respuesta["on_account_payment_control"]);
            $("#editarResto").val(respuesta["substraction_payment_control"]);
            $("#idControlPago").val(respuesta["id_payment_control"]);
        }
    });
});

$(document).on("click", ".btnEliminarControlPago", function() {
    var idControlPago = $(this).attr("idControlPago");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el control de pago?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar control de pago!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=control-pagos&idControlPago=" + idControlPago;
        }
    });
});