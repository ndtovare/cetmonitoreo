$(document).on("click", ".btnEditarChofer", function() {
    var idChofer = $(this).attr("idChofer");
    var datos = new FormData();
    datos.append("idChofer", idChofer);

    $.ajax({
        url: "ajax/choferes.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#editarNombreChofer").val(respuesta["name_owner_driver_client"]);
            $("#editarApellidoChofer").val(respuesta["last_name_owner_driver_client"]);
            $("#editarTelefonoChofer").val(respuesta["phone_owner_driver_client"]);
            $("#editarIdentificacionChofer").val(respuesta["identification_owner_driver_client"]);
            $("#editarNumeroIdentificacionChofer").val(respuesta["identification_number_owner_driver_client"]);
            $("#editarDireccionChofer").val(respuesta["address_owner_driver_client"]);
            $("#editarNombreReferenciaChofer").val(respuesta["reference_name_owner_driver_client"]);
            $("#editarApellidoReferenciaChofer").val(respuesta["reference_last_name_owner_driver_client"]);
            $("#editarTelefonoReferenciaChofer").val(respuesta["reference_phone_owner_driver_client"]);
            $("#idChofer").val(respuesta["id_client"]);
        }
    });
});

$(document).on("click", ".btnEliminarChofer", function() {
    var idChofer = $(this).attr("idChofer");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el chofer?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar chofer!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=choferes&idChofer=" + idChofer;
        }
    });
});