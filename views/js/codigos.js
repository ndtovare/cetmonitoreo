$(".nuevaFoto").change(function() {
    var imagen = this.files[0];

    // Validar el formato de la imagen, sea jpg o png
    if(imagen["type"] != "image/jpeg" && imagen["type"] !== "image/png") {
        $(".nuevaFoto").val("");

        Swal.fire({
            icon: "error",
            title: "Error al subir la imagen",
            text: "¡La imagen debe estar en formato JPG o PNG!",
            confirmButtonText: "Cerrar"
        });
    } else if(imagen["size"] > 2000000) {
        $(".nuevaFoto").val("");

        Swal.fire({
            icon: "error",
            title: "Error al subir la imagen",
            text: "¡La imagen no debe pesar más de 2MB!",
            confirmButtonText: "Cerrar"
        });
    } else {
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event) {
            var rutaImagen = event.target.result;
            $(".previsualizar").attr("src", rutaImagen);
        });
    }
});

$(document).on("click", ".btnEditarCodigo", function() {
    var idCodigo = $(this).attr("idCodigo");
    var datos = new FormData();
    datos.append("idCodigo", idCodigo);

    $.ajax({
        url: "ajax/codigos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#editarCodigo").val(respuesta["name_code"]);
            $("#editarDescripcion").val(respuesta["description_code"]);
            $("#editarPrecio").val(respuesta["price_code"]);
            $("#idCodigo").val(respuesta["id_code"]);

            if(respuesta["image_user"] != "") {
                $(".previsualizar").attr("src", respuesta["image_code"]);
            } else {
                $(".previsualizar").attr("src", "views/img/productos/default/anonymous.png");
            }
        }
    });
});

$(document).on("click", ".btnEliminarCodigo", function() {
    var idCodigo = $(this).attr("idCodigo");
    var fotoCodigo = $(this).attr("fotoCodigo");
    var codigo = $(this).attr("codigo");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el código?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar código!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=codigos&idCodigo=" + idCodigo + "&codigo=" + codigo + "&fotoCodigo=" + fotoCodigo;
        }
    });
});