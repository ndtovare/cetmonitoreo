var numeroEconomico = $('#nuevoEconomicoVehiculo');

$(document).ready(function() {
    numeroEconomico.hide();
});

$(document).on("change", "#nuevoTipoVehiculo", function() {
    var nuevoVehiculo = $('#nuevoTipoVehiculo').val();
    if(nuevoVehiculo == "Transporte público") {
        numeroEconomico.show();
    } else if(nuevoVehiculo == "Vehículo privado") {
        numeroEconomico.hide();
    } else {
        numeroEconomico.hide();
    }
});