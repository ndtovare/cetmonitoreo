$(document).on("click", ".btnEditarTransportePublico", function() {
    var idTransportePublico = $(this).attr("idTransportePublico");
    console.log(idTransportePublico);
    var datos = new FormData();
    datos.append("idTransportePublico", idTransportePublico);

    $.ajax({
        url: "ajax/transportes-publicos.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            console.log(respuesta);
            $("#editarImeiTP").val(respuesta["imei_private_public_vehicle"]);
            $("#editarIdentificadorTP").val(respuesta["identifier_private_public_vehicle"]);
            $("#editarMarcaTP").val(respuesta["brand_private_public_vehicle"]);
            $("#editarModeloTP").val(respuesta["model_private_public_vehicle"]);
            $("#editarAnioTP").val(respuesta["year_private_public_vehicle"]);
            $("#editarSerieTP").val(respuesta["vehicular_series_private_public_vehicle"]);
            $("#editarEconomicoTP").val(respuesta["economic_number_public_vehicle"]);
            $("#editarPlacaTP").val(respuesta["plate_number_private_public_vehicle"]);
            $("#editarTipoTP").val(respuesta["class_private_public_vehicle"]);
            $("#editarPropietario").val(respuesta["id_owner"]);
            $("#editarChofer").val(respuesta["id_driver"]);
            $("#editarCompania").val(respuesta["id_business"]);
            $("#idTransportePublico").val(respuesta["id_vehicle"]);
        }
    });
});

$(document).on("click", ".btnEliminarTransportePublico", function() {
    var idTransportePublico = $(this).attr("idTransportePublico");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el transporte público?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar transporte público!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=transportes-publicos&idTransportePublico=" + idTransportePublico;
        }
    });
});