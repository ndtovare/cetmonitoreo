var publicoCompania = $('#nuevoTransportePublicoCompania');
var privadoCompania = $('#nuevoVehiculoPrivadoCompania');
var editarpublicoCompania = $('#editarTransportePublicoCompania');
var editarprivadoCompania = $('#editarVehiculoPrivadoCompania');

$(document).ready(function() {
    publicoCompania.hide();
    privadoCompania.hide();
    editarpublicoCompania.hide();
    editarprivadoCompania.hide();
});

$(document).on("click", ".btnEditarCompania", function() {
    var idCompania = $(this).attr("idCompania");
    var datos = new FormData();
    datos.append("idCompania", idCompania);

    $.ajax({
        url: "ajax/companias.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            var editarVehiculoSeleccionado = $("#editarSeleccionaVehiculoC");
            console.log(respuesta);

            if(respuesta["type_vehicle"] == "Transporte público") {
                editarVehiculoSeleccionado.val("TP");
            } else if(respuesta["type_vehicle"] == "Vehículo privado") {
                editarVehiculoSeleccionado.val("VP");
            }

            if(editarVehiculoSeleccionado.val() == "TP") {
                editarpublicoCompania.show();
                editarprivadoCompania.hide();
                $("#editarTransportePublico").val(respuesta["id_vehicle"]);
            } else if(editarVehiculoSeleccionado.val() == "VP") {
                editarprivadoCompania.show();
                editarpublicoCompania.hide();
                $("#editarVehiculoPrivado").val(respuesta["id_vehicle"]);
            }

            $("#editarNombreCompania").val(respuesta["name_business"]);
            $("#editarAcronimoCompania").val(respuesta["acronym_business"]);
            $("#editarTelefonoCompania").val(respuesta["phone_business"]);
            $("#editarDireccionCompania").val(respuesta["address_business"]);
            $("#editarCorreoCompania").val(respuesta["email_business"]);
            $("#editarNombreRepresentanteCompania").val(respuesta["representative_name_business"]);
            $("#editarApellidoRepresentanteCompania").val(respuesta["representative_last_name_business"]);
            $("#editarTelefonoRepresentanteCompania").val(respuesta["representative_phone_business"]);
            $("#editarCorreoRepresentanteCompania").val(respuesta["representative_email_business"]);
            $("#editarNombreReferenciaCompania").val(respuesta["reference_name_business"]);
            $("#editarApellidoReferenciaCompania").val(respuesta["reference_last_name_business"]);
            $("#editarTelefonoReferenciaCompania").val(respuesta["reference_phone_business"]);
            $("#idCompania").val(respuesta["id_business"]);
        }
    });
});

$(document).on("click", ".btnEliminarCompania", function() {
    var idCompania = $(this).attr("idCompania");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar la compañía?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar compañía!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=companias&idCompania=" + idCompania;
        }
    });
});

$(document).on("change", "#seleccionaVehiculoCompania", function() {
    var vehiculoSeleccionado = $('#seleccionaVehiculoCompania').val();
    console.log(vehiculoSeleccionado);
    if(vehiculoSeleccionado == "TP") {
        publicoCompania.show();
        privadoCompania.hide();
    } else if(vehiculoSeleccionado == "VP") {
        privadoCompania.show();
        publicoCompania.hide();
    }
});