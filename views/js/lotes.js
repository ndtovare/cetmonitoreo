$(document).on("click", ".btnEditarLote", function() {
    var idLote = $(this).attr("idLote");
    var datos = new FormData();
    datos.append("idLote", idLote);

    $.ajax({
        url: "ajax/lotes.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#idLote").val(respuesta["id_lot"]);
            $("#editarFactura").val(respuesta["bill_lot"]);
            $("#editarFechaAdmision").val(respuesta["date_admission_lot"]);
            $("#editarFechaGarantia").val(respuesta["warranty_date_lot"]);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

$(document).on("click", ".btnEliminarLote", function() {
    var idLote = $(this).attr("idLote");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el lote?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar lote!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=lotes&idLote=" + idLote;
        }
    });
});