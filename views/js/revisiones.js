$(document).on("click", ".btnEditarRevision", function() {
    var idRevision = $(this).attr("idRevision");
    var datos = new FormData();
    datos.append("idRevision", idRevision);

    $.ajax({
        url: "ajax/revisiones.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#idRevision").val(respuesta["id_revision"]);
            $("#editarComentario").val(respuesta["comments_revision"]);
            $("#editarFechaRevision").val(respuesta["date_revision"]);
            $("#editarPrecio").val(respuesta["price_revision"]);
            $("#editarIdentificador").val(respuesta["identifier_revision"]);
            $("#editarEquipoInstalado").val(respuesta["id_installed_equipment"]);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

$(document).on("click", ".btnEliminarRevision", function() {
    var idRevision = $(this).attr("idRevision");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar la revisión?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar revisión!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=revisiones&idRevision=" + idRevision;
        }
    });
});