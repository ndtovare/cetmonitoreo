var publicoFlotilla = $('#nuevoTransportePublicoFlotilla');
var privadoFlotilla = $('#nuevoVehiculoPrivadoFlotilla');
var editarpublicoFlotilla = $('#editarTransportePublicoFlotilla');
var editarprivadoFlotilla = $('#editarVehiculoPrivadoFlotilla');

$(document).ready(function() {
    publicoFlotilla.hide();
    privadoFlotilla.hide();
    editarpublicoFlotilla.hide();
    editarprivadoFlotilla.hide();
});

$(document).on("click", ".btnEditarFlotilla", function() {
    var idFlotilla = $(this).attr("idFlotilla");
    var datos = new FormData();
    datos.append("idFlotilla", idFlotilla);

    $.ajax({
        url: "ajax/flotillas.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            var editarVehiculoSeleccionado = $("#editarSeleccionaVehiculoF");
            console.log(respuesta);

            if(respuesta["type_vehicle"] == "Transporte público") {
                editarVehiculoSeleccionado.val("TP");
            } else if(respuesta["type_vehicle"] == "Vehículo privado") {
                editarVehiculoSeleccionado.val("VP");
            }

            if(editarVehiculoSeleccionado.val() == "TP") {
                editarpublicoFlotilla.show();
                editarprivadoFlotilla.hide();
                $("#editarTransportePublico").val(respuesta["id_vehicle"]);
            } else if(editarVehiculoSeleccionado.val() == "VP") {
                editarprivadoFlotilla.show();
                editarpublicoFlotilla.hide();
                $("#editarVehiculoPrivado").val(respuesta["id_vehicle"]);
            }

            $("#editarNombreFlotilla").val(respuesta["name_fleet"]);
            $("#editarDireccionFlotilla").val(respuesta["address_fleet"]);
            $("#editarNombreContactoFlotilla").val(respuesta["name_contact_fleet"]);
            $("#editarApellidoContactoFlotilla").val(respuesta["last_name_contact_fleet"]);
            $("#editarTelefonoFlotilla").val(respuesta["phone_contact_fleet"]);
            $("#editarCompania").val(respuesta["id_business"]);
            $("#idFlotilla").val(respuesta["id_fleet"]);
        }
    });
});

$(document).on("click", ".btnEliminarFlotilla", function() {
    var idFlotilla = $(this).attr("idFlotilla");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar la flotilla?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar flotilla!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=flotillas&idFlotilla=" + idFlotilla;
        }
    });
});

$(document).on("change", "#seleccionaVehiculoFlotilla", function() {
    var vehiculoSeleccionado = $('#seleccionaVehiculoFlotilla').val();
    console.log(vehiculoSeleccionado);
    if(vehiculoSeleccionado == "TP") {
        publicoFlotilla.show();
        privadoFlotilla.hide();
    } else if(vehiculoSeleccionado == "VP") {
        privadoFlotilla.show();
        publicoFlotilla.hide();
    }
});