$(".nuevaFoto").change(function() {
    var imagen = this.files[0];

    // Validar el formato de la imagen, sea jpg o png
    if(imagen["type"] != "image/jpeg" && imagen["type"] !== "image/png") {
        $(".nuevaFoto").val("");

        Swal.fire({
            icon: "error",
            title: "Error al subir la imagen",
            text: "¡La imagen debe estar en formato JPG o PNG!",
            confirmButtonText: "Cerrar"
        });
    } else if(imagen["size"] > 2000000) {
        $(".nuevaFoto").val("");

        Swal.fire({
            icon: "error",
            title: "Error al subir la imagen",
            text: "¡La imagen no debe pesar más de 2MB!",
            confirmButtonText: "Cerrar"
        });
    } else {
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event) {
            var rutaImagen = event.target.result;
            $(".previsualizar").attr("src", rutaImagen);
        });
    }
});

$(document).on("click", ".btnEditarUsuario", function() {
    var idUsuario = $(this).attr("idUsuario");
    var datos = new FormData();
    datos.append("idUsuario", idUsuario);

    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            console.log(respuesta);
            $("#editarNombre").val(respuesta["name_user"]);
            $("#editarApellido").val(respuesta["last_name_user"]);
            $("#editarUsuario").val(respuesta["nickname_user"]);
            $("#editarCorreo").val(respuesta["email_user"]);
            $("#editarTelefono").val(respuesta["phone_user"]);
            $("#editarRol").val(respuesta["role_user"]);
            $("#editarContrasenia").val(respuesta["password_user"]);
            $("#direccionActual").val(respuesta["address_user"]);
            $("#nombreReferenciaActual").val(respuesta["reference_name_user"]);
            $("#telefonoReferenciaActual").val(respuesta["reference_phone_user"]);
            $("#fotoActual").val(respuesta["image_user"]);
            $("#idUsuario").val(idUsuario);

            if(respuesta["image_user"] != "") {
                $(".previsualizar").attr("src", respuesta["image_user"]);
            } else {
                $(".previsualizar").attr("src", "views/img/usuarios/default/anonymous.png");
            }
        }
    });
});

$(document).on("click", ".btnEliminarUsuario", function() {
    var idUsuario = $(this).attr("idUsuario");
    var fotoUsuario = $(this).attr("fotoUsuario");
    var usuario = $(this).attr("usuario");

    Swal.fire({
        icon: "warning",
        title: "¿Está seguro de borrar el usuario?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Cancelar",
        confirmButtonText: "¡Si, borrar usuario!"
    }).then((result) => {
        if(result.value) {
            window.location = "index.php?ruta=usuarios&idUsuario=" + idUsuario + "&usuario=" + usuario + "&fotoUsuario=" + fotoUsuario;
        }
    });
});